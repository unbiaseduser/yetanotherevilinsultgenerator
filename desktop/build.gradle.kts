import org.beryx.jlink.JlinkZipTask

plugins {
  id("java")
  //noinspection JavaPluginLanguageLevel
  id("application")
  kotlin("jvm")
  id("org.javamodularity.moduleplugin") version "1.8.15"
  id("org.openjfx.javafxplugin") version "0.1.0"
  id("org.beryx.jlink") version "3.0.1"
}

group = "com.sixtyninefourtwenty.yetanotherevilinsultgenerator.desktop"
version = "1.0"

tasks.withType<JavaCompile>().configureEach {
  options.encoding = "UTF-8"
}

application {
  //mainModule.set("com.sixtyninefourtwenty.yetanotherevilinsultgenerator.desktop")
  mainClass.set("com.sixtyninefourtwenty.yetanotherevilinsultgenerator.desktop.MyApplication")
}

kotlin {
  jvmToolchain(17)
}

javafx {
  version = "17.0.7"
  modules = listOf("javafx.controls", "javafx.fxml")
}

dependencies {
  implementation(project(":shared"))
  implementation(libs.materialfx)
  implementation(libs.kotlinx.coroutines.javafx)
  testImplementation(libs.junit.jupiter.api)
  testRuntimeOnly(libs.junit.jupiter.engine)
}

tasks.withType<Test>().configureEach {
  useJUnitPlatform()
}

jlink {
  imageZip.set(project.file("${layout.buildDirectory.asFile.get().path}/distributions/app-${javafx.platform.classifier}.zip"))
  options.set(listOf("--strip-debug", "--compress", "2", "--no-header-files", "--no-man-pages"))
  launcher {
    name = "app"
  }
}

tasks.withType<JlinkZipTask>().configureEach {
  group = "distribution"
}