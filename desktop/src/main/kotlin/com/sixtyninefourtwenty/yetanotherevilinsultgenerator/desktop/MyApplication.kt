package com.sixtyninefourtwenty.yetanotherevilinsultgenerator.desktop

import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.desktop.generator.GeneratorViewModel
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.desktop.main.MainView
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.desktop.main.MainViewModel
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.desktop.offline.OfflineViewModel
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.desktop.utils.fxmlLoader
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.desktop.utils.resourceBundle
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.OfficialOnlineInsultGeneratorService
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.database.CustomGeneratorInfoDatabase
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.database.OfflineInsultsDatabase
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.datasource.InsultsLocalDataSourceImpl
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.datasource.InsultsRemoteDataSourceImpl
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.repository.CustomGeneratorInfoRepository
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.repository.CustomGeneratorInfoRepositoryImpl
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.repository.InsultsRepository
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.repository.InsultsRepositoryImpl
import javafx.application.Application
import javafx.scene.Scene
import javafx.stage.Stage
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob

class MyApplication : Application() {

    private val scope = CoroutineScope(SupervisorJob())
    private lateinit var database: OfflineInsultsDatabase
    private lateinit var customGeneratorInfoRepository: CustomGeneratorInfoRepository
    private lateinit var insultsRepository: InsultsRepository
    private lateinit var mainView: MainView
    private lateinit var generatorViewModel: GeneratorViewModel
    private lateinit var offlineViewModel: OfflineViewModel

    override fun init() {
        database = OfflineInsultsDatabase.newInstance()
        val localDataSource = InsultsLocalDataSourceImpl(database)
        customGeneratorInfoRepository = CustomGeneratorInfoRepositoryImpl(
            CustomGeneratorInfoDatabase.newInstance(), scope
        )
        insultsRepository = InsultsRepositoryImpl(
            listOf(
                InsultsRemoteDataSourceImpl(
                    { resourceBundle("misc").getString("official.service.display.name") },
                    OfficialOnlineInsultGeneratorService(OfficialOnlineInsultGeneratorService.Url.MAIN)
                ),
                InsultsRemoteDataSourceImpl(
                    { resourceBundle("misc").getString("official.service.display.backup.name") },
                    OfficialOnlineInsultGeneratorService(OfficialOnlineInsultGeneratorService.Url.BACKUP)
                )
            ),
            customGeneratorInfoRepository,
            localDataSource,
            scope
        )
        generatorViewModel = GeneratorViewModel(insultsRepository)
        offlineViewModel = OfflineViewModel(insultsRepository)
    }

    override fun start(stage: Stage) {
        val res = resourceBundle("misc")
        val rootView = fxmlLoader("main").apply {
            setControllerFactory { _ -> MainView(MainViewModel(), generatorViewModel, offlineViewModel).also { mainView = it } }
            load()
        }.getController<MainView>()

        with(stage) {
            title = res.getString("app.name")
            scene = Scene(rootView.root, 800.0, 500.0)
            show()
        }
    }

    override fun stop() {
        database.close()
        mainView.close()
    }

}

fun main() {
    Application.launch(MyApplication::class.java)
}