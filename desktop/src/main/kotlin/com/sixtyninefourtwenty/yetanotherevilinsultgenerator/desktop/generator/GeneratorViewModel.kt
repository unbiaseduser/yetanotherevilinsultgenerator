package com.sixtyninefourtwenty.yetanotherevilinsultgenerator.desktop.generator

import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.ViewState
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.Language
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.dto.InsultDto
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.repository.InsultsRepository
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.utils.asExceptionOrThrow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow

class GeneratorViewModel(private val insultsRepository: InsultsRepository) {

    private val mutableViewState = MutableStateFlow<ViewState<InsultDto>>(ViewState.Empty)
    val viewState = mutableViewState.asStateFlow()

    suspend fun generateRemoteInsult(language: Language, id: String) {
        mutableViewState.value = ViewState.Loading
        runCatching {
            insultsRepository.generateRemoteInsult(id, language)
        }.onFailure {
            mutableViewState.value = ViewState.Error(it.asExceptionOrThrow())
        }.onSuccess {
            upsertInsult(it)
            mutableViewState.value = ViewState.Content(it)
        }
    }

    suspend fun upsertInsult(insult: InsultDto) = insultsRepository.upsertInsult(insult)

}