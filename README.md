# Yet Another Evil Insult Generator
Another frontend app for the [Evil Insult Generator](https://evilinsult.com)

## Features
- Access exquisite, high quality insults (as long as you have internet)
- Save insults for offline usage, to be able to vanquish your nemesis no matter where you are - be
  it in the middle of nowhere, in a nuclear fallout shelter, or just in your toilet scrolling Reddit
  posts. (We've all been there.)
- Mark offline-d insults as favorite, so you always have on your fingertip the best-of-the-best
  counter to the oddly specific social scenario you just made up.

## Releases
*TBD.*

## Disclaimer
This is not an official Evil Insult Generator project. Their official app can be found [here](https://github.com/EvilInsultGenerator/android-app).