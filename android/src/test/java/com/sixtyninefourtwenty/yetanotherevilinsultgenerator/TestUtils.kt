package com.sixtyninefourtwenty.yetanotherevilinsultgenerator

import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.Language
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.dto.InsultDto
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.cancel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch
import kotlinx.datetime.LocalDate
import kotlinx.datetime.LocalDateTime
import kotlinx.datetime.LocalTime
import java.time.Month
import java.util.UUID

private object NULL

@Suppress("UNCHECKED_CAST")
suspend fun <T> Flow<T>.lastAndAwait(): T {
    var data: Any? = NULL
    CoroutineScope(Dispatchers.Default).launch {
        collect {
            data = it
            cancel()
        }
    }.join()
    return data as T
}

fun createInsultDto(number: Int) = InsultDto(
    number = number,
    language = Language.entries.random(),
    insult = UUID.randomUUID().toString(),
    created = LocalDateTime(LocalDate(1, Month.JANUARY, 1), LocalTime(1, 1)),
    shown = 1,
    createdBy = null,
    active = 1,
    comment = null,
    isFavorite = false,
    rating = null
)