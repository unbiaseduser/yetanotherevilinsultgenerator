package com.sixtyninefourtwenty.yetanotherevilinsultgenerator

import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.Language
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.dto.InsultDto
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.sortandfilter.InsultFilterOptions
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.sortandfilter.InsultSortOptions
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.sortandfilter.TextFilter
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.sortandfilter.sortAndFilter
import org.junit.jupiter.api.Assertions.assertIterableEquals
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import java.util.stream.Stream

class InsultSortAndFilterTest {

    @ParameterizedTest
    @MethodSource("sortArgs")
    fun sort(initialList: List<InsultDto>, sortOptions: InsultSortOptions, expectedList: List<InsultDto>) {
        val newData = initialList.sortAndFilter(
            sortOptions = sortOptions,
            filterOptions = null
        )
        assertIterableEquals(expectedList, newData)
    }

    @ParameterizedTest
    @MethodSource("filterArgs")
    fun filter(initialList: List<InsultDto>, filterOptions: InsultFilterOptions, expectedList: List<InsultDto>) {
        val newData = initialList.sortAndFilter(
            sortOptions = null,
            filterOptions = filterOptions
        )
        assertIterableEquals(expectedList, newData)
    }

    companion object {
        @JvmStatic
        fun filterArgs(): Stream<Arguments> {
            val initialList1 = listOf(
                createInsultDto(1).copy(language = Language.BENGALI),
                createInsultDto(1).copy(language = Language.ENGLISH),
                createInsultDto(1).copy(language = Language.ENGLISH)
            )
            val filterOptions1 = InsultFilterOptions(
                language = Language.ENGLISH,
                textFilter = null
            )
            val expectedList1 = listOf(
                initialList1[1], initialList1[2]
            )
            val initialList2 = listOf(
                createInsultDto(1).copy(insult = "foo"),
                createInsultDto(1).copy(insult = "bar"),
                createInsultDto(1).copy(insult = "baz")
            )
            val filterOptions2 = InsultFilterOptions(
                language = null,
                textFilter = TextFilter(
                    textToSearch = "b",
                    mode = TextFilter.Mode.CONTAINS
                )
            )
            val expectedList2 = listOf(
                initialList2[1], initialList2[2]
            )
            val initialList3 = listOf(
                createInsultDto(1).copy(insult = "foo"),
                createInsultDto(1).copy(insult = "bar"),
                createInsultDto(1).copy(insult = "baz")
            )
            val filterOptions3 = InsultFilterOptions(
                language = null,
                textFilter = TextFilter(
                    textToSearch = "b",
                    mode = TextFilter.Mode.NOT_CONTAINS
                )
            )
            val expectedList3 = listOf(
                initialList3[0]
            )
            val initialList4 = listOf(
                createInsultDto(1).copy(insult = "foo", language = Language.ENGLISH),
                createInsultDto(1).copy(insult = "bar", language = Language.BENGALI),
                createInsultDto(1).copy(insult = "baz", language = Language.ENGLISH)
            )
            val filterOptions4 = InsultFilterOptions(
                language = Language.ENGLISH,
                textFilter = TextFilter(
                    textToSearch = "b",
                    mode = TextFilter.Mode.CONTAINS
                )
            )
            val expectedList4 = listOf(
                initialList4[2]
            )
            val initialList5 = listOf(
                createInsultDto(1).copy(insult = "foo", language = Language.ENGLISH),
                createInsultDto(1).copy(insult = "bar", language = Language.BENGALI),
                createInsultDto(1).copy(insult = "baz", language = Language.ENGLISH)
            )
            val filterOptions5 = InsultFilterOptions(
                language = Language.ENGLISH,
                textFilter = TextFilter(
                    textToSearch = "b",
                    mode = TextFilter.Mode.NOT_CONTAINS
                )
            )
            val expectedList5 = listOf(
                initialList5[0]
            )
            return Stream.of(
                Arguments.of(initialList1, filterOptions1, expectedList1),
                Arguments.of(initialList2, filterOptions2, expectedList2),
                Arguments.of(initialList3, filterOptions3, expectedList3),
                Arguments.of(initialList4, filterOptions4, expectedList4),
                Arguments.of(initialList5, filterOptions5, expectedList5)
            )
        }
        @JvmStatic
        fun sortArgs(): Stream<Arguments> {
            val initialList1 = listOf(
                createInsultDto(1).copy(insult = "b"),
                createInsultDto(1).copy(insult = "a"),
                createInsultDto(1).copy(insult = "c")
            )
            val sortOptions1 = InsultSortOptions(
                favoritesFirst = false,
                mode = InsultSortOptions.Mode.CONTENT_ALPHABETICAL_ASCENDING
            )
            val expectedList1 = listOf(
                initialList1[1], initialList1[0], initialList1[2]
            )
            val initialList2 = listOf(
                createInsultDto(1).copy(insult = "b"),
                createInsultDto(1).copy(insult = "a"),
                createInsultDto(1).copy(insult = "c")
            )
            val sortOptions2 = InsultSortOptions(
                favoritesFirst = false,
                mode = InsultSortOptions.Mode.CONTENT_ALPHABETICAL_DESCENDING
            )
            val expectedList2 = listOf(
                initialList2[2], initialList2[0], initialList2[1]
            )
            val initialList3 = listOf(
                createInsultDto(1).copy(insult = "b", isFavorite = true),
                createInsultDto(1).copy(insult = "a"),
                createInsultDto(1).copy(insult = "c", isFavorite = true)
            )
            val sortOptions3 = InsultSortOptions(
                favoritesFirst = true,
                mode = null
            )
            val expectedList3 = listOf(
                initialList3[0], initialList3[2], initialList3[1]
            )
            val initialList4 = listOf(
                createInsultDto(1).copy(insult = "b", isFavorite = true),
                createInsultDto(1).copy(insult = "a", isFavorite = true),
                createInsultDto(1).copy(insult = "c"),
                createInsultDto(1).copy(insult = "e"),
                createInsultDto(1).copy(insult = "d", isFavorite = true)
            )
            val sortOptions4 = InsultSortOptions(
                favoritesFirst = true,
                mode = InsultSortOptions.Mode.CONTENT_ALPHABETICAL_ASCENDING
            )
            val expectedList4 = listOf(
                initialList4[1],
                initialList4[0],
                initialList4[4],
                initialList4[2],
                initialList4[3]
            )
            val initialList5 = listOf(
                createInsultDto(1).copy(insult = "b", isFavorite = true),
                createInsultDto(1).copy(insult = "a", isFavorite = true),
                createInsultDto(1).copy(insult = "c"),
                createInsultDto(1).copy(insult = "e"),
                createInsultDto(1).copy(insult = "d", isFavorite = true)
            )
            val sortOptions5 = InsultSortOptions(
                favoritesFirst = true,
                mode = InsultSortOptions.Mode.CONTENT_ALPHABETICAL_DESCENDING
            )
            val expectedList5 = listOf(
                initialList5[4],
                initialList5[0],
                initialList5[1],
                initialList5[3],
                initialList5[2]
            )
            return Stream.of(
                Arguments.of(initialList1, sortOptions1, expectedList1),
                Arguments.of(initialList2, sortOptions2, expectedList2),
                Arguments.of(initialList3, sortOptions3, expectedList3),
                Arguments.of(initialList4, sortOptions4, expectedList4),
                Arguments.of(initialList5, sortOptions5, expectedList5)
            )
        }
    }

}