package com.sixtyninefourtwenty.yetanotherevilinsultgenerator

import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.content.pm.ActivityInfo
import androidx.appcompat.app.AppCompatActivity
import androidx.test.core.app.ActivityScenario
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.intent.Intents
import androidx.test.espresso.intent.Intents.intended
import androidx.test.espresso.intent.matcher.IntentMatchers.hasAction
import androidx.test.espresso.intent.matcher.IntentMatchers.hasExtra
import androidx.test.espresso.intent.matcher.IntentMatchers.hasType
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.Language
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.dto.InsultDto
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.utils.copyToClipboard
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.utils.share
import kotlinx.datetime.toKotlinLocalDateTime
import org.hamcrest.Matchers.allOf
import org.hamcrest.Matchers.`is`
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.robolectric.Shadows.shadowOf
import tech.apter.junit.jupiter.robolectric.RobolectricExtension
import java.time.LocalDateTime

@ExtendWith(RobolectricExtension::class)
class InsultUtilsTest {

    private val insult = InsultDto(
        number = 1,
        language = Language.ENGLISH,
        insult = "insult",
        created = LocalDateTime.now().toKotlinLocalDateTime(),
        shown = 1,
        createdBy = null,
        active = 1,
        comment = null,
        isFavorite = false,
        rating = null
    )

    @Test
    fun share() {
        Intents.init()
        val context = ApplicationProvider.getApplicationContext<Context>()
        val activityInfo = ActivityInfo().apply {
            theme = androidx.appcompat.R.style.Theme_AppCompat
            name = AppCompatActivity::class.java.name
            packageName = context.packageName
        }
        shadowOf(context.packageManager).addOrUpdateActivity(activityInfo)
        ActivityScenario.launch(AppCompatActivity::class.java).use {
            it.onActivity { activity ->
                insult.share(activity)
                intended(allOf(
                    hasAction(Intent.ACTION_CHOOSER),
                    hasExtra(Intent.EXTRA_INTENT, allOf(
                        hasAction(Intent.ACTION_SEND),
                        hasType("text/plain"),
                        hasExtra(Intent.EXTRA_TEXT, `is`(insult.insult))
                    ))
                ))
            }
        }
        Intents.release()
    }

    @Test
    fun copyToClipboard() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        insult.copyToClipboard(context)
        val cm = context.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        assertEquals(insult.insult, cm.primaryClip!!.getItemAt(0).text)
    }

}