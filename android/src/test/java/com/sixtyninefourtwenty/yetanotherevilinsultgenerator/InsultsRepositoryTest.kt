package com.sixtyninefourtwenty.yetanotherevilinsultgenerator

import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.OFFICIAL_EIG_BACKUP_URL
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.OFFICIAL_EIG_URL
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.datasource.InsultsLocalDataSource
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.datasource.InsultsRemoteDataSource
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.Language
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.dto.GeneratorInfo
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.dto.InsultDto
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.dto.InsultRatingDto
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.dto.OnlineGeneratorInfo
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.repository.CustomGeneratorInfoRepository
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.repository.InsultsRepository
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.repository.InsultsRepositoryImpl
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.runTest
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertNotEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class InsultsRepositoryTest {

    private lateinit var fakeInsultsLocalDataSource: InsultsLocalDataSource
    private lateinit var fakeInsultsRemoteDataSource1: InsultsRemoteDataSource
    private lateinit var fakeInsultsRemoteDataSource2: InsultsRemoteDataSource
    private lateinit var fakeCustomGeneratorInfoRepository: CustomGeneratorInfoRepository

    private lateinit var insultsRepository: InsultsRepository

    private val remoteInsults1 = listOf(
        createInsultDto(4),
        createInsultDto(5),
        createInsultDto(6)
    )
    private val remoteInsults2 = listOf(
        createInsultDto(7),
        createInsultDto(8),
        createInsultDto(9)
    )
    private val initialLocalInsults = listOf(
        createInsultDto(1),
        createInsultDto(2),
        createInsultDto(3)
    )

    @BeforeEach
    fun setUp() {
        fakeInsultsRemoteDataSource1 = FakeInsultsRemoteDataSource(OFFICIAL_EIG_URL, remoteInsults1)
        fakeInsultsRemoteDataSource2 = FakeInsultsRemoteDataSource(OFFICIAL_EIG_BACKUP_URL, remoteInsults2)
        fakeInsultsLocalDataSource = FakeInsultsLocalDataSource(initialLocalInsults)
        fakeCustomGeneratorInfoRepository = FakeCustomGeneratorInfoRepository()
        insultsRepository = InsultsRepositoryImpl(
            listOf(fakeInsultsRemoteDataSource1, fakeInsultsRemoteDataSource2),
            fakeCustomGeneratorInfoRepository,
            fakeInsultsLocalDataSource,
            CoroutineScope(SupervisorJob())
        )
    }

    @Test
    fun insertInsult() = runTest {
        val newInsult = createInsultDto(10)
        insultsRepository.upsertInsult(newInsult)
        assertTrue(insultsRepository.allAvailableLocalInsults.lastAndAwait().contains(newInsult))
    }

    @Test
    fun updateInsult() = runTest {
        val newInsultText = "edited insult"
        val editedInsult = insultsRepository.allAvailableLocalInsults.lastAndAwait().random().copy(
            insult = newInsultText
        )
        insultsRepository.upsertInsult(editedInsult)
        assertEquals(newInsultText, insultsRepository.allAvailableLocalInsults.lastAndAwait()
            .first { it.number == editedInsult.number }.insult)
    }

    @Test
    fun deleteLocalInsult() = runTest {
        val insults = insultsRepository.allAvailableLocalInsults.lastAndAwait()
        val deletedInsult = insults.random()
        insultsRepository.deleteLocalInsult(deletedInsult)
        val newInsults = insultsRepository.allAvailableLocalInsults.lastAndAwait()
        assertEquals(insults.size - 1, newInsults.size)
        assertFalse(newInsults.contains(deletedInsult))
    }

    @Test
    fun deleteLocalInsults() = runTest {
        val insults = insultsRepository.allAvailableLocalInsults.lastAndAwait()
        val firstDeletedInsult = insults.random()
        val secondDeletedInsult = (insults - firstDeletedInsult).random()
        insultsRepository.deleteLocalInsults(listOf(
            firstDeletedInsult,
            secondDeletedInsult
        ))
        val newInsults = insultsRepository.allAvailableLocalInsults.lastAndAwait()
        assertEquals(insults.size - 2, newInsults.size)
        assertFalse(newInsults.contains(firstDeletedInsult))
        assertFalse(newInsults.contains(secondDeletedInsult))
    }

    @Test
    fun updateFavoriteStatus() = runTest {
        val insult = insultsRepository.allAvailableLocalInsults.lastAndAwait().random()
        val newFavoriteStatus = !insult.isFavorite
        insultsRepository.updateFavoriteStatus(insult, newFavoriteStatus)
        assertNotEquals(insult.isFavorite, insultsRepository.allAvailableLocalInsults.lastAndAwait()
            .first { it.number == insult.number }.isFavorite)
    }

    @Test
    fun getFavoriteStatus() = runTest {
        val insult = insultsRepository.allAvailableLocalInsults.lastAndAwait().random()
        assertEquals(insult.isFavorite, insultsRepository.getFavoriteStatus(insult).lastAndAwait())
    }

    @Test
    fun updateRating() = runTest {
        val insult = insultsRepository.allAvailableLocalInsults.lastAndAwait().random()
        val newRating = InsultRatingDto(rating = 1F, explanation = null)
        insultsRepository.updateRating(insult, newRating)
        assertNotEquals(insult.rating, insultsRepository.allAvailableLocalInsults.lastAndAwait()
            .first { it.number == insult.number }.rating)
    }

    @Test
    fun getRating() = runTest {
        val insult = insultsRepository.allAvailableLocalInsults.lastAndAwait().random()
        assertEquals(insult.rating, insultsRepository.getRating(insult).lastAndAwait())
    }

    @Test
    fun generateRemoteInsult() = runTest {
        val generatedInsult1 = insultsRepository.generateRemoteInsult(OFFICIAL_EIG_URL, Language.ENGLISH)
        assertTrue(remoteInsults1.contains(generatedInsult1))
        val generatedInsult2 = insultsRepository.generateRemoteInsult(OFFICIAL_EIG_BACKUP_URL, Language.ENGLISH)
        assertTrue(remoteInsults2.contains(generatedInsult2))

        val infoId = "i"
        val insultForCustomGenerator = createInsultDto(100)
        val mockWebServer = MockWebServer()
        with(mockWebServer) {
            start()
            enqueue(MockResponse().setBody(insultForCustomGenerator.toJson()))
        }
        fakeCustomGeneratorInfoRepository.upsertOnlineInfo(
            OnlineGeneratorInfo(
                id = infoId,
                baseUrl = mockWebServer.url("/").toString(),
                displayName = "",
                languageParameterName = null,
                extraQueryParams = mapOf()
            )
        )
        assertEquals(insultForCustomGenerator, insultsRepository.generateRemoteInsult(infoId, Language.ENGLISH))
        mockWebServer.shutdown()
    }

}

private class FakeCustomGeneratorInfoRepository : CustomGeneratorInfoRepository {

    private val _allOnlineInfo = MutableStateFlow<List<OnlineGeneratorInfo>>(listOf())

    override val allOnlineInfo: Flow<List<OnlineGeneratorInfo>> = _allOnlineInfo.asStateFlow()

    override suspend fun getAllOnlineInfoSnapshot(): List<OnlineGeneratorInfo> {
        return _allOnlineInfo.value
    }

    override suspend fun upsertOnlineInfo(info: OnlineGeneratorInfo) {
        val existingIndex = _allOnlineInfo.value.indexOfFirst { it.id == info.id }
        if (existingIndex >= 0) {
            _allOnlineInfo.value = _allOnlineInfo.value.toMutableList().apply {
                this[existingIndex] = info
            }
        } else {
            _allOnlineInfo.value += info
        }
    }

    override suspend fun deleteOnlineInfo(id: String) {
        val existing = _allOnlineInfo.value.firstOrNull { it.id == id }
        if (existing != null) {
            _allOnlineInfo.value -= existing
        }
    }

}

private class FakeInsultsRemoteDataSource(
    override val id: String,
    private val insults: List<InsultDto>
) : InsultsRemoteDataSource {

    override val dynamicDisplayName: () -> String = { id }

    override val constantDisplayName: String?
        get() = null

    override suspend fun generateRemoteInsult(language: Language): InsultDto {
        return insults.random()
    }

    override fun getInfo(): GeneratorInfo {
        TODO("Not yet implemented")
    }

}

private class FakeInsultsLocalDataSource(initialInsults: List<InsultDto>) : InsultsLocalDataSource {

    private val data = mutableListOf<InsultDto>()

    private val favoriteFlows = mutableMapOf<Int, MutableStateFlow<Boolean>>()
    private val ratingFlows = mutableMapOf<Int, MutableStateFlow<InsultRatingDto?>>()
    private val _allAvailableInsults: MutableStateFlow<List<InsultDto>> = MutableStateFlow(java.util.List.copyOf(data))

    override val allAvailableInsults: StateFlow<List<InsultDto>> = _allAvailableInsults.asStateFlow()

    init {
        runBlocking {
            initialInsults.forEach { upsertInsult(it) }
        }
    }

    private fun modifyData(block: () -> Unit) {
        block()
        _allAvailableInsults.value = java.util.List.copyOf(data)
    }

    override suspend fun upsertInsult(insult: InsultDto) = modifyData {
        val existing = data.firstOrNull { it.number == insult.number }
        if (existing != null) {
            data[data.indexOf(existing)] = insult
            favoriteFlows.getOrPut(insult.number) { MutableStateFlow(insult.isFavorite) }.value = insult.isFavorite
            ratingFlows.getOrPut(insult.number) { MutableStateFlow(insult.rating) }.value = insult.rating
        } else {
            data.add(insult)
            favoriteFlows[insult.number] = MutableStateFlow(insult.isFavorite)
            ratingFlows[insult.number] = MutableStateFlow(insult.rating)
        }
    }

    override suspend fun deleteInsult(number: Int) = modifyData {
        data.removeIf { it.number == number }
        favoriteFlows.remove(number)
        ratingFlows.remove(number)
    }

    override suspend fun deleteInsults(numbers: IntArray) = modifyData {
        data.removeIf { numbers.contains(it.number) }
        numbers.forEach {
            favoriteFlows.remove(it)
            ratingFlows.remove(it)
        }
    }

    override suspend fun updateFavoriteStatus(number: Int, isFavorite: Boolean) = modifyData {
        val existing = data.firstOrNull { it.number == number }
        if (existing != null) {
            data[data.indexOf(existing)] = existing.copy(isFavorite = isFavorite)
            favoriteFlows[number]!!.value = isFavorite
        }
    }

    override fun getFavoriteStatus(number: Int): Flow<Boolean> {
        return favoriteFlows[number] ?: throw IllegalArgumentException("Insult with number $number doesn't exist")
    }

    override suspend fun updateRating(number: Int, rating: InsultRatingDto?) = modifyData {
        val existing = data.firstOrNull { it.number == number }
        if (existing != null) {
            data[data.indexOf(existing)] = existing.copy(rating = rating)
            ratingFlows[number]!!.value = rating
        }
    }

    override fun getRating(number: Int): Flow<InsultRatingDto?> {
        return ratingFlows[number] ?: throw IllegalArgumentException("Insult with number $number doesn't exist")
    }

}