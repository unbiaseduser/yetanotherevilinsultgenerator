package com.sixtyninefourtwenty.yetanotherevilinsultgenerator

import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.OnlineInsultGeneratorService
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.Insult
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.Language
import kotlinx.coroutines.test.runTest
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import java.util.stream.Stream

class OnlineInsultGeneratorServiceTest {

    private lateinit var mockWebServer: MockWebServer
    private lateinit var service: OnlineInsultGeneratorService

    @BeforeEach
    fun setUp() {
        mockWebServer = MockWebServer()
        mockWebServer.start()
        service = FakeOnlineInsultGeneratorService(mockWebServer.url("/").toString())
    }

    @AfterEach
    fun tearDown() {
        mockWebServer.shutdown()
    }

    @ParameterizedTest
    @MethodSource("testArgs")
    fun test(response: String, expected: Insult) = runTest {
        mockWebServer.enqueue(MockResponse().setBody(response))
        assertEquals(expected, service.generate(Language.ENGLISH))
    }

    companion object {
        @JvmStatic
        fun testArgs(): Stream<Arguments> {
            return Stream.of(
                Arguments.of(
                    """
                    {
                        "number": "1",
                        "language": "en",
                        "insult": "foo",
                        "created": "2024-04-10 01:02:03",
                        "shown": "1",
                        "createdby": "someone",
                        "active": "2",
                        "comment": "cmt"
                    }
                    """.trimIndent(),
                    Insult().apply {
                        number = 1
                        language = "en"
                        insult = "foo"
                        created = "2024-04-10 01:02:03"
                        shown = 1
                        createdBy = "someone"
                        active = 2
                        comment = "cmt"
                    }
                ),
                Arguments.of(
                    """
                        {
                            "number": "2",
                            "language": "es",
                            "insult": "bar",
                            "created": "2023-06-09 04:06:11",
                            "shown": "2",
                            "createdby": "",
                            "active": "3",
                            "comment": "comment"
                        }
                    """.trimIndent(),
                    Insult().apply {
                        number = 2
                        language = "es"
                        insult = "bar"
                        created = "2023-06-09 04:06:11"
                        shown = 2
                        createdBy = null
                        active = 3
                        comment = "comment"
                    }
                ),
                Arguments.of(
                    """
                        {
                            "number": "3",
                            "language": "cn",
                            "insult": "baz",
                            "created": "2022-01-02 17:06:11",
                            "shown": "3",
                            "createdby": "c",
                            "active": "4",
                            "comment": ""
                        }
                    """.trimIndent(),
                    Insult().apply {
                        number = 3
                        language = "cn"
                        insult = "baz"
                        created = "2022-01-02 17:06:11"
                        shown = 3
                        createdBy = "c"
                        active = 4
                        comment = null
                    }
                )
            )
        }
    }

}

private class FakeOnlineInsultGeneratorService(baseUrl: String) : OnlineInsultGeneratorService(
    id = baseUrl,
    baseUrl = baseUrl,
    languageParameterName = "",
    extraQueryParams = mapOf()
)