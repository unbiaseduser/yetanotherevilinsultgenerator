package com.sixtyninefourtwenty.yetanotherevilinsultgenerator

import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.OFFICIAL_EIG_BACKUP_URL
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.OFFICIAL_EIG_URL
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.ViewState
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.datasource.InsultsRemoteDataSource
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.Language
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.dto.InsultDto
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.dto.InsultRatingDto
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.repository.InsultsRepository
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.ui.generator.GeneratorViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertInstanceOf
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class GeneratorViewModelTest {

    companion object {
        @OptIn(ExperimentalCoroutinesApi::class)
        @JvmStatic
        @BeforeAll
        fun setUpAll() {
            Dispatchers.setMain(StandardTestDispatcher())
        }
    }

    private lateinit var vm: GeneratorViewModel
    private lateinit var insultsRepository: FakeInsultsRepository
    private val remoteInsults1 = listOf(
        createInsultDto(1),
        createInsultDto(2),
        createInsultDto(3)
    )
    private val remoteInsults2 = listOf(
        createInsultDto(4),
        createInsultDto(5),
        createInsultDto(6)
    )

    @BeforeEach
    fun setUp() {
        insultsRepository = FakeInsultsRepository(
            listOf(OFFICIAL_EIG_URL to remoteInsults1, OFFICIAL_EIG_BACKUP_URL to remoteInsults2)
        )
        vm = GeneratorViewModel(insultsRepository = insultsRepository)
    }

    @Test
    fun upsertInsult() = runTest {
        val oldSize = insultsRepository.data().size
        val insult = createInsultDto(10)
        vm.upsertInsult(insult).join()
        val newInsults = insultsRepository.data()
        assertTrue(newInsults.contains(insult))
        assertEquals(oldSize + 1, newInsults.size)
    }

    @Test
    fun generateRemoteInsultSuccess() = runTest {
        vm.generateRemoteInsult(Language.ENGLISH, OFFICIAL_EIG_URL).join()
        val state1 = assertInstanceOf(ViewState.Content::class.java, vm.viewState.value)
        assertTrue(remoteInsults1.contains(state1.data as InsultDto))
        vm.generateRemoteInsult(Language.ENGLISH, OFFICIAL_EIG_BACKUP_URL).join()
        val state2 = assertInstanceOf(ViewState.Content::class.java, vm.viewState.value)
        assertTrue(remoteInsults2.contains(state2.data as InsultDto))
    }

    @Test
    fun generateRemoteInsultFailure() = runTest {
        insultsRepository.generateRemoteInsultSuccessfully = false
        vm.generateRemoteInsult(Language.ENGLISH, OFFICIAL_EIG_URL).join()
        assertInstanceOf(ViewState.Error::class.java, vm.viewState.value)
        vm.generateRemoteInsult(Language.ENGLISH, OFFICIAL_EIG_BACKUP_URL).join()
        assertInstanceOf(ViewState.Error::class.java, vm.viewState.value)
    }

}

private class FakeInsultsRepository(private val remoteInsults: List<Pair<String, List<InsultDto>>>) : InsultsRepository {

    override val allCustomOnlineGenerators: Flow<List<InsultsRemoteDataSource>>
        get() = TODO("Not yet implemented")

    override val officialRemoteDataSources: List<InsultsRemoteDataSource>
        get() = TODO("Not yet implemented")

    var generateRemoteInsultSuccessfully: Boolean = true

    private val _data = mutableListOf<InsultDto>()

    fun data(): List<InsultDto> = java.util.List.copyOf(_data)

    override val allAvailableLocalInsults: Flow<List<InsultDto>>
        get() = TODO("Not yet implemented")

    override suspend fun deleteLocalInsult(number: Int) {
        TODO("Not yet implemented")
    }

    override suspend fun deleteLocalInsults(numbers: IntArray) {
        TODO("Not yet implemented")
    }

    override suspend fun upsertInsult(insult: InsultDto) {
        val existing = _data.firstOrNull { it.number == insult.number }
        if (existing != null) {
            _data[_data.indexOf(existing)] = insult
        } else {
            _data.add(insult)
        }
    }

    override suspend fun updateFavoriteStatus(number: Int, isFavorite: Boolean) {
        TODO("Not yet implemented")
    }

    override fun getFavoriteStatus(number: Int): Flow<Boolean> {
        TODO("Not yet implemented")
    }

    override fun getRating(number: Int): Flow<InsultRatingDto?> {
        TODO("Not yet implemented")
    }

    override suspend fun updateRating(number: Int, rating: InsultRatingDto?) {
        TODO("Not yet implemented")
    }

    override suspend fun generateRemoteInsult(id: String, language: Language): InsultDto {
        return if (generateRemoteInsultSuccessfully) {
            remoteInsults.first { id == it.first }.second.random()
        } else {
            throw IllegalStateException("generateRemoteInsultSuccessfully is false")
        }
    }

}