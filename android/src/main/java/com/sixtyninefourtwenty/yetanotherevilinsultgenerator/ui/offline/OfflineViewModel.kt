package com.sixtyninefourtwenty.yetanotherevilinsultgenerator.ui.offline

import androidx.annotation.MainThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProvider.AndroidViewModelFactory.Companion.APPLICATION_KEY
import androidx.lifecycle.asLiveData
import androidx.lifecycle.createSavedStateHandle
import androidx.lifecycle.map
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.initializer
import androidx.lifecycle.viewmodel.viewModelFactory
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.MyApplication
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.data.sortandfilter.ParcelableInsultFilterOptions
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.data.sortandfilter.ParcelableInsultSortOptions
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.dto.InsultDto
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.repository.InsultsRepository
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.sortandfilter.InsultFilterOptions
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.sortandfilter.InsultSortOptions
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.sortandfilter.sortAndFilter
import kotlinx.coroutines.launch

class OfflineViewModel(
    private val repository: InsultsRepository,
    ssh: SavedStateHandle
) : ViewModel() {

    private val allAvailableInsultsLiveData = repository.allAvailableLocalInsults.asLiveData()
    private val mutableParcelableInsultFilterOptionsLiveData: MutableLiveData<ParcelableInsultFilterOptions?> = ssh.getLiveData(INSULT_FILTER_OPTIONS_STATE_KEY)
    private val insultFilterOptionsLiveData = mutableParcelableInsultFilterOptionsLiveData.map { it?.obj }
    private val mutableParcelableInsultSortOptionsLiveData: MutableLiveData<ParcelableInsultSortOptions?> = ssh.getLiveData(INSULT_SORT_OPTIONS_STATE_KEY)
    private val insultSortOptionsLiveData = mutableParcelableInsultSortOptionsLiveData.map { it?.obj }
    private val insultsAfterSearchAndFilterMediatorLiveData = MediatorLiveData<List<InsultDto>>().apply {
        addSource(allAvailableInsultsLiveData) { insults ->
            value = insults.sortAndFilter(insultSortOptionsLiveData.value, insultFilterOptionsLiveData.value)
        }
        addSource(insultFilterOptionsLiveData) {
            val insults = allAvailableInsultsLiveData.value
            value = insults?.sortAndFilter(insultSortOptionsLiveData.value, it) ?: listOf()
        }
        addSource(insultSortOptionsLiveData) {
            val insults = allAvailableInsultsLiveData.value
            value = insults?.sortAndFilter(it, insultFilterOptionsLiveData.value) ?: listOf()
        }
    }
    val insultsAfterSearchAndFilter: LiveData<List<InsultDto>> = insultsAfterSearchAndFilterMediatorLiveData

    @set:MainThread //required by LiveData.setValue()
    var insultFilterOptions: InsultFilterOptions?
        get() = insultFilterOptionsLiveData.value
        set(value) {
            mutableParcelableInsultFilterOptionsLiveData.value = value?.let { ParcelableInsultFilterOptions(it) }
        }

    @set:MainThread
    var insultSortOptions: InsultSortOptions?
        get() = insultSortOptionsLiveData.value
        set(value) {
            mutableParcelableInsultSortOptionsLiveData.value = value?.let { ParcelableInsultSortOptions(it) }
        }

    fun deleteLocalInsults(insults: Iterable<InsultDto>) = viewModelScope.launch {
        repository.deleteLocalInsults(insults)
    }

    companion object {
        private const val INSULT_FILTER_OPTIONS_STATE_KEY = "ifo"
        private const val INSULT_SORT_OPTIONS_STATE_KEY = "iso"
        val Factory: ViewModelProvider.Factory = viewModelFactory {
            initializer {
                val repo = (get(APPLICATION_KEY) as MyApplication).insultsRepository
                OfflineViewModel(repo, createSavedStateHandle())
            }
        }
    }
}