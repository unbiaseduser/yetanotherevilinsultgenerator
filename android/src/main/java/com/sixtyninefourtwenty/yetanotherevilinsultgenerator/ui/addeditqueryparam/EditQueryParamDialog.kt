package com.sixtyninefourtwenty.yetanotherevilinsultgenerator.ui.addeditqueryparam

import android.os.Bundle
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.LifecycleOwner
import androidx.navigation.fragment.navArgs
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.R

class EditQueryParamDialog : AbstractAddEditQueryParamDialog() {

    companion object {
        private const val EDIT_PARAM_REQUEST_KEY = "edit_param"
        private const val OLD_PARAM_NAME_KEY = "old_param_name"
        private const val NEW_PARAM_NAME_KEY = "new_param_name"
        private const val OLD_PARAM_VALUE_KEY = "old_param_value"
        private const val NEW_PARAM_VALUE_KEY = "new_param_value"
        fun registerCallback(
            activity: FragmentActivity,
            lifecycleOwner: LifecycleOwner,
            callback: (oldName: String, newName: String, oldValue: String, newValue: String) -> Unit
        ) {
            activity.supportFragmentManager.setFragmentResultListener(
                EDIT_PARAM_REQUEST_KEY,
                lifecycleOwner
            ) { _, result ->
                callback(
                    result.getString(OLD_PARAM_NAME_KEY)!!,
                    result.getString(NEW_PARAM_NAME_KEY)!!,
                    result.getString(OLD_PARAM_VALUE_KEY)!!,
                    result.getString(NEW_PARAM_VALUE_KEY)!!
                )
            }
        }
    }

    private val args: EditQueryParamDialogArgs by navArgs()

    override val param: Pair<String, String>
        get() = args.queryName to args.queryValue
    override val title: Int
        get() = R.string.edit_parameter

    override fun onParamCreated(name: String, value: String) {
        requireActivity().supportFragmentManager.setFragmentResult(
            EDIT_PARAM_REQUEST_KEY,
            Bundle().apply {
                putString(OLD_PARAM_NAME_KEY, args.queryName)
                putString(NEW_PARAM_NAME_KEY, name)
                putString(OLD_PARAM_VALUE_KEY, args.queryValue)
                putString(NEW_PARAM_VALUE_KEY, value)
            }
        )
    }

}