package com.sixtyninefourtwenty.yetanotherevilinsultgenerator.ui.generator

import android.content.ClipData
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.net.ConnectivityManager
import android.os.Bundle
import android.util.TypedValue
import android.view.View
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.view.updateLayoutParams
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.findNavController
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.kennyc.view.MultiStateView
import com.sixtyninefourtwenty.basefragments.ViewBindingFragment
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.R
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.databinding.FragmentGeneratorBinding
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.databinding.FragmentGeneratorStateContentBinding
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.databinding.FragmentGeneratorStateEmptyBinding
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.databinding.FragmentGeneratorStateErrorBinding
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.ViewState
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.Language
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.utils.clipboardManager
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.utils.languageLabelResId
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.utils.preferenceRepository
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.utils.setContentView
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.utils.setEmptyView
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.utils.setErrorView
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.utils.tint
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.launch

class GeneratorFragment : ViewBindingFragment<FragmentGeneratorBinding>(FragmentGeneratorBinding::inflate) {

    private val generatorViewModel: GeneratorViewModel by viewModels { GeneratorViewModel.Factory }
    private val prefs by preferenceRepository()

    private var _emptyBinding: FragmentGeneratorStateEmptyBinding? = null
    private val emptyBinding get() = _emptyBinding!!
    private var _contentBinding: FragmentGeneratorStateContentBinding? = null
    private val contentBinding get() = _contentBinding!!
    private var _errorBinding: FragmentGeneratorStateErrorBinding? = null
    private val errorBinding get() = _errorBinding!!

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val binding = requireBinding()
        prefs.generatorLanguageLiveData.observe(viewLifecycleOwner) {
            binding.generatorLanguage.text = getString(it.languageLabelResId)
        }
        binding.generatorLanguage.setOnClickListener {
            MaterialAlertDialogBuilder(requireContext())
                .setTitle(R.string.language)
                .setSingleChoiceItems(
                    Language.entries.map { getString(it.languageLabelResId) }.toTypedArray(),
                    Language.entries.indexOf(prefs.generatorLanguage)
                ) { dialog, which ->
                    prefs.generatorLanguage = Language.entries[which]
                    dialog.dismiss()
                }
                .setNegativeButton(android.R.string.cancel, null)
                .show()
        }
        with(binding.manageGenerators) {
            setOnClickListener {
                if (prefs.enableCustomGenerators) {
                    findNavController().navigate(GeneratorFragmentDirections.actionNavigationGeneratorToNavigationManageGenerators())
                } else {
                    findNavController().navigate(GeneratorFragmentDirections.actionNavigationGeneratorToNavigationPickOfficialGenerator())
                }
            }
        }
        val connectivityManager = requireContext().getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                generatorViewModel.checkNetworkAvailability(connectivityManager).distinctUntilChanged().collect {
                    if (it) {
                        with(binding.networkNotification) {
                            setChipIconResource(R.drawable.public1)
                            text = getString(R.string.online)
                        }
                    } else {
                        with(binding.networkNotification) {
                            chipIcon = ContextCompat.getDrawable(requireContext(), R.drawable.public1_off)?.tint(Color.RED)
                            text = getString(R.string.offline)
                        }
                    }
                }
            }
        }
        _emptyBinding = FragmentGeneratorStateEmptyBinding.inflate(layoutInflater)
        _contentBinding = FragmentGeneratorStateContentBinding.inflate(layoutInflater)
        _errorBinding = FragmentGeneratorStateErrorBinding.inflate(layoutInflater)
        with(binding.stateView) {
            setEmptyView(emptyBinding.root)
            setContentView(contentBinding.root)
            setErrorView(errorBinding.root)
            viewState = MultiStateView.ViewState.EMPTY
        }
        emptyBinding.generate.setOnClickListener {
            generateInsult()
        }
        contentBinding.generate.setOnClickListener {
            generateInsult()
        }
        contentBinding.share.setOnClickListener {
            startActivity(Intent.createChooser(Intent(Intent.ACTION_SEND)
                .putExtra(Intent.EXTRA_TEXT, contentBinding.insult.text)
                .setType("text/plain"), null))
        }
        contentBinding.copyToClipboard.setOnClickListener {
            requireContext().clipboardManager.setPrimaryClip(ClipData.newPlainText("generated_insult", contentBinding.insult.text))
            Toast.makeText(requireContext(), R.string.copied_to_clipboard, Toast.LENGTH_SHORT).show()
        }
        errorBinding.retry.setOnClickListener {
            generateInsult()
        }
        contentBinding.root.post {
            contentBinding.insultCard.updateLayoutParams<ConstraintLayout.LayoutParams> {
                matchConstraintMaxHeight = contentBinding.root.height - (contentBinding.buttons.height +
                        TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10F, resources.displayMetrics).toInt())
            }
        }
        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
                generatorViewModel.viewState.collect {
                    when (it) {
                        is ViewState.Empty -> binding.stateView.viewState = MultiStateView.ViewState.EMPTY
                        is ViewState.Error -> binding.stateView.viewState = MultiStateView.ViewState.ERROR
                        is ViewState.Loading -> binding.stateView.viewState = MultiStateView.ViewState.LOADING
                        is ViewState.Content -> {
                            binding.stateView.viewState = MultiStateView.ViewState.CONTENT
                            contentBinding.insult.text = it.data.insult
                        }
                    }
                }
            }
        }

    }

    private fun generateInsult() {
        generatorViewModel.generateRemoteInsult(prefs.generatorLanguage, prefs.generatorId)
    }

    override fun onDestroyView(binding: FragmentGeneratorBinding) {
        _emptyBinding = null
        _contentBinding = null
        _errorBinding = null
    }

}