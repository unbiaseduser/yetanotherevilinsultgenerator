package com.sixtyninefourtwenty.yetanotherevilinsultgenerator.ui.adapter

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.recyclerview.selection.ItemDetailsLookup
import androidx.recyclerview.selection.ItemDetailsLookup.ItemDetails
import androidx.recyclerview.selection.ItemKeyProvider
import androidx.recyclerview.selection.SelectionTracker
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.R
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.databinding.ListItemInsultBinding
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.dto.InsultDto
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.utils.tint

class InsultAdapter(private val onInsultClick: (InsultDto) -> Unit) : ListAdapter<InsultDto, InsultAdapter.ViewHolder>(INSULT_DIFFER) {

    lateinit var selectionTracker: SelectionTracker<Long>

    fun getSelectedItems() = currentList.filter {
        selectionTracker.isSelected(it.number.toLong())
    }

    private var favoritesDrawable: Drawable? = null

    private fun getFavoritesDrawable(context: Context) = favoritesDrawable ?: ContextCompat.getDrawable(context, R.drawable.favorite)?.tint(Color.RED).also {
        favoritesDrawable = it
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ListItemInsultBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding, getFavoritesDrawable(parent.context)) { onInsultClick(getItem(it)) }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position), selectionTracker)
    }

    class DetailsLookup(private val recyclerView: RecyclerView) : ItemDetailsLookup<Long>() {
        override fun getItemDetails(e: MotionEvent): ItemDetails<Long>? {
            return recyclerView.findChildViewUnder(e.x, e.y)?.let {
                (recyclerView.getChildViewHolder(it) as? ViewHolder)?.getItemDetails()
            }
        }
    }

    inner class KeyProvider : ItemKeyProvider<Long>(SCOPE_CACHED) {
        override fun getKey(position: Int): Long {
            return getItem(position).number.toLong()
        }

        override fun getPosition(key: Long): Int {
            return currentList.indexOfFirst { it.number.toLong() == key }
        }
    }

    class ViewHolder(
        private val binding: ListItemInsultBinding,
        private val favoritesDrawable: Drawable?,
        private val onInsultClick: (Int) -> Unit
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(insult: InsultDto, selectionTracker: SelectionTracker<Long>) {
            binding.root.isChecked = selectionTracker.isSelected(insult.number.toLong())
            binding.insult.text = insult.insult

            binding.favoriteIcon.chipIcon = favoritesDrawable
            if (insult.isFavorite) {
                binding.favoriteIcon.isVisible = true
                binding.root.setContentPadding(
                    binding.root.contentPaddingLeft,
                    0,
                    binding.root.contentPaddingRight,
                    binding.root.contentPaddingBottom
                )
            } else {
                binding.favoriteIcon.isVisible = false
                binding.root.setContentPadding(
                    binding.root.contentPaddingLeft,
                    binding.root.contentPaddingBottom,
                    binding.root.contentPaddingRight,
                    binding.root.contentPaddingBottom
                )
            }
        }

        fun getItemDetails() = object : ItemDetails<Long>() {
            override fun getPosition(): Int = absoluteAdapterPosition

            override fun getSelectionKey(): Long? {
                val adapter = bindingAdapter as? InsultAdapter
                return adapter?.getItem(absoluteAdapterPosition)?.number?.toLong()
            }

        }

        init {
            binding.root.setOnClickListener { onInsultClick(absoluteAdapterPosition) }
        }
    }

    companion object {
        private val INSULT_DIFFER = object : DiffUtil.ItemCallback<InsultDto>() {
            override fun areItemsTheSame(oldItem: InsultDto, newItem: InsultDto): Boolean {
                return oldItem.number == newItem.number
            }

            override fun areContentsTheSame(oldItem: InsultDto, newItem: InsultDto): Boolean {
                return oldItem == newItem
            }

        }
    }
}