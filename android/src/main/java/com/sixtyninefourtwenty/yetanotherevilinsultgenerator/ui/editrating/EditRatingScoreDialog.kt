package com.sixtyninefourtwenty.yetanotherevilinsultgenerator.ui.editrating

import android.os.Bundle
import android.view.View
import androidx.core.os.BundleCompat
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.LifecycleOwner
import androidx.navigation.fragment.navArgs
import com.sixtyninefourtwenty.bottomsheetalertdialog.BottomSheetAlertDialogFragmentViewBuilder
import com.sixtyninefourtwenty.bottomsheetalertdialog.DialogButtonProperties
import com.sixtyninefourtwenty.bottomsheetalertdialog.misc.ViewBindingBottomSheetAlertDialogFragment
import com.sixtyninefourtwenty.bottomsheetalertdialog.misc.createBottomSheetAlertDialog
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.R
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.data.model.ParcelableInsultRatingDto
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.databinding.DialogEditRatingScoreBinding
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.dto.InsultRatingDto

class EditRatingScoreDialog : ViewBindingBottomSheetAlertDialogFragment<DialogEditRatingScoreBinding>(DialogEditRatingScoreBinding::inflate) {

    private val args: EditRatingScoreDialogArgs by navArgs()
    private val rating: InsultRatingDto by lazy { args.rating.obj }

    companion object {
        private const val EDIT_RATING_SCORE_REQUEST_KEY = "edit_rating_score"
        private const val RATING_KEY = "rating"
        fun registerCallback(
            activity: FragmentActivity,
            lifecycleOwner: LifecycleOwner,
            callback: (InsultRatingDto) -> Unit
        ) {
            activity.supportFragmentManager.setFragmentResultListener(
                EDIT_RATING_SCORE_REQUEST_KEY,
                lifecycleOwner
            ) { _, result ->
                callback(
                    BundleCompat.getParcelable(result, RATING_KEY, ParcelableInsultRatingDto::class.java)!!.obj
                )
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val binding = requireBinding()
        binding.root.value = rating.rating
    }

    override fun createDialog(binding: DialogEditRatingScoreBinding): BottomSheetAlertDialogFragmentViewBuilder {
        return createBottomSheetAlertDialog(
            view = binding.root,
            titleText = getString(R.string.rating),
            positiveButtonProperties = DialogButtonProperties(
                text = getString(android.R.string.ok),
                listener = {
                    requireActivity().supportFragmentManager.setFragmentResult(
                        EDIT_RATING_SCORE_REQUEST_KEY,
                        Bundle().apply {
                            putParcelable(RATING_KEY, ParcelableInsultRatingDto(InsultRatingDto(
                                rating = binding.root.value,
                                explanation = rating.explanation
                            )))
                        }
                    )
                }
            ),
            negativeButtonProperties = DialogButtonProperties(
                text = getString(android.R.string.cancel)
            )
        )
    }

}