package com.sixtyninefourtwenty.yetanotherevilinsultgenerator.ui.managegenerators

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.initializer
import androidx.lifecycle.viewmodel.viewModelFactory
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.MyApplication
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.dto.OnlineGeneratorInfo
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.repository.CustomGeneratorInfoRepository
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.repository.InsultsRepository
import kotlinx.coroutines.launch

class ManageGeneratorsViewModel(
    insultsRepository: InsultsRepository,
    private val customGeneratorInfoRepository: CustomGeneratorInfoRepository
) : ViewModel() {

    val officialRemoteDataSources = insultsRepository.officialRemoteDataSources
    val allCustomOnlineGenerators = insultsRepository.allCustomOnlineGenerators

    fun upsertCustomOnlineGeneratorInfo(info: OnlineGeneratorInfo) = viewModelScope.launch {
        customGeneratorInfoRepository.upsertOnlineInfo(info)
    }

    fun deleteCustomOnlineGeneratorInfo(id: String) = viewModelScope.launch {
        customGeneratorInfoRepository.deleteOnlineInfo(id)
    }

    companion object {
        val Factory: ViewModelProvider.Factory = viewModelFactory {
            initializer {
                val app = get(ViewModelProvider.AndroidViewModelFactory.APPLICATION_KEY) as MyApplication
                ManageGeneratorsViewModel(app.insultsRepository, app.customGeneratorInfoRepository)
            }
        }
    }

}