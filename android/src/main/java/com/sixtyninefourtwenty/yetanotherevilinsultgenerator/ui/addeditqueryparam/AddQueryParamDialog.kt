package com.sixtyninefourtwenty.yetanotherevilinsultgenerator.ui.addeditqueryparam

import android.os.Bundle
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.LifecycleOwner
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.R

class AddQueryParamDialog : AbstractAddEditQueryParamDialog() {

    companion object {
        private const val ADD_PARAM_REQUEST_KEY = "add_param"
        private const val PARAM_NAME_KEY = "param_name"
        private const val PARAM_VALUE_KEY = "param_value"
        fun registerCallback(
            activity: FragmentActivity,
            lifecycleOwner: LifecycleOwner,
            callback: (name: String, value: String) -> Unit
        ) {
            activity.supportFragmentManager.setFragmentResultListener(
                ADD_PARAM_REQUEST_KEY,
                lifecycleOwner
            ) { _, result ->
                callback(
                    result.getString(PARAM_NAME_KEY)!!,
                    result.getString(PARAM_VALUE_KEY)!!
                )
            }
        }
    }

    override val param: Pair<String, String>? = null
    override val title: Int
        get() = R.string.add_parameter

    override fun onParamCreated(name: String, value: String) {
        requireActivity().supportFragmentManager.setFragmentResult(
            ADD_PARAM_REQUEST_KEY,
            Bundle().apply {
                putString(PARAM_NAME_KEY, name)
                putString(PARAM_VALUE_KEY, value)
            }
        )
    }

}