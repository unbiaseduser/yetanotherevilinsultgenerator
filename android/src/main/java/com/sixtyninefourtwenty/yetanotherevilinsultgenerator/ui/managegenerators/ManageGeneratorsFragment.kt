package com.sixtyninefourtwenty.yetanotherevilinsultgenerator.ui.managegenerators

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.widget.PopupMenu
import androidx.core.net.toUri
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.sixtyninefourtwenty.basefragments.ViewBindingFragment
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.R
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.data.model.ParcelableOnlineGeneratorInfo
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.databinding.FragmentManageGeneratorsBinding
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.dto.OnlineGeneratorInfo
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.ui.adapter.GeneratorAdapter
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.ui.addeditcustomonlinegenerator.AddCustomOnlineGeneratorDialog
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.ui.addeditcustomonlinegenerator.EditCustomOnlineGeneratorDialog
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.utils.myApplication
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.utils.startActivitySafely
import kotlinx.coroutines.launch

class ManageGeneratorsFragment : ViewBindingFragment<FragmentManageGeneratorsBinding>(FragmentManageGeneratorsBinding::inflate) {

    private lateinit var officialGeneratorsAdapter: GeneratorAdapter
    private lateinit var customGeneratorsAdapter: GeneratorAdapter
    private val viewModel: ManageGeneratorsViewModel by viewModels { ManageGeneratorsViewModel.Factory }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val binding = requireBinding()
        binding.eigStatusPage.setOnClickListener {
            startActivitySafely(Intent(Intent.ACTION_VIEW).setData("https://stats.uptimerobot.com/NLnZ8sVro".toUri())) {
                Toast.makeText(requireContext(), R.string.no_browser_app_installed_error, Toast.LENGTH_SHORT).show()
            }
        }
        val prefs = requireContext().myApplication.preferencesRepository
        officialGeneratorsAdapter = GeneratorAdapter(
            prefs.generatorId,
            onItemClick = { ds, a ->
                prefs.generatorId = ds.id
                a.selectedId = ds.id
                customGeneratorsAdapter.selectedId = ds.id
            },
            onMenuClick = null
        )
        customGeneratorsAdapter = GeneratorAdapter(
            prefs.generatorId,
            onItemClick = { ds, a ->
                prefs.generatorId = ds.id
                a.selectedId = ds.id
                officialGeneratorsAdapter.selectedId = ds.id
            },
            onMenuClick = { ds, v ->
                PopupMenu(requireContext(), v).apply {
                    setForceShowIcon(true)
                    menu.add(R.string.delete)
                        .setIcon(R.drawable.delete)
                        .setOnMenuItemClickListener {
                            MaterialAlertDialogBuilder(requireContext())
                                .setTitle(R.string.delete)
                                .setMessage(getString(R.string.delete_generator_prompt, ds.getDisplayName()))
                                .setPositiveButton(android.R.string.ok) { _, _ ->
                                    if (prefs.generatorId == ds.id) {
                                        prefs.coerceGeneratorIdToOfficial()
                                        val newGeneratorId = prefs.generatorId
                                        officialGeneratorsAdapter.selectedId = newGeneratorId
                                        customGeneratorsAdapter.selectedId = newGeneratorId
                                    }
                                    viewModel.deleteCustomOnlineGeneratorInfo(ds.id)
                                }
                                .setNegativeButton(android.R.string.cancel, null)
                                .show()
                            true
                        }
                    menu.add(R.string.edit)
                        .setIcon(R.drawable.edit)
                        .setOnMenuItemClickListener {
                            findNavController().navigate(ManageGeneratorsFragmentDirections.actionNavigationManageGeneratorsToNavigationEditCustomOnlineGenerator(
                                ParcelableOnlineGeneratorInfo(ds.getInfo() as OnlineGeneratorInfo)
                            ))
                            true
                        }
                }.show()
            }
        )
        with(binding.officialGenerators) {
            layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
            adapter = officialGeneratorsAdapter
        }
        with(binding.customGenerators) {
            layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
            adapter = customGeneratorsAdapter
        }
        officialGeneratorsAdapter.submitList(viewModel.officialRemoteDataSources)
        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.allCustomOnlineGenerators.collect { customOnlineGenerators ->
                    customGeneratorsAdapter.submitList(customOnlineGenerators)
                }
            }
        }
        binding.addCustomOnlineGenerator.setOnClickListener {
            findNavController().navigate(ManageGeneratorsFragmentDirections.actionNavigationManageGeneratorsToNavigationAddCustomOnlineGenerator())
        }

        AddCustomOnlineGeneratorDialog.registerCallback(
            requireActivity(),
            viewLifecycleOwner
        ) { info ->
            viewModel.upsertCustomOnlineGeneratorInfo(info)
        }

        EditCustomOnlineGeneratorDialog.registerCallback(
            requireActivity(),
            viewLifecycleOwner
        ) { _, newInfo ->
            viewModel.upsertCustomOnlineGeneratorInfo(newInfo)
        }

    }

}