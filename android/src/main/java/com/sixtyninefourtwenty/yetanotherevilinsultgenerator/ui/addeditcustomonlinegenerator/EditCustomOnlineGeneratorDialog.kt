package com.sixtyninefourtwenty.yetanotherevilinsultgenerator.ui.addeditcustomonlinegenerator

import android.os.Bundle
import androidx.core.os.BundleCompat
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.LifecycleOwner
import androidx.navigation.NavDirections
import androidx.navigation.fragment.navArgs
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.R
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.data.model.ParcelableOnlineGeneratorInfo
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.dto.OnlineGeneratorInfo

class EditCustomOnlineGeneratorDialog : AbstractAddEditCustomOnlineGeneratorDialog() {

    companion object {
        private const val EDIT_GENERATOR_REQUEST_KEY = "edit_generator"
        private const val OLD_INFO_KEY = "old_info"
        private const val NEW_INFO_KEY = "new_info"
        fun registerCallback(
            activity: FragmentActivity,
            lifecycleOwner: LifecycleOwner,
            callback: (oldInfo: OnlineGeneratorInfo, newInfo: OnlineGeneratorInfo) -> Unit
        ) {
            activity.supportFragmentManager.setFragmentResultListener(
                EDIT_GENERATOR_REQUEST_KEY,
                lifecycleOwner
            ) { _, result ->
                callback(
                    BundleCompat.getParcelable(result, OLD_INFO_KEY, ParcelableOnlineGeneratorInfo::class.java)!!.obj,
                    BundleCompat.getParcelable(result, NEW_INFO_KEY, ParcelableOnlineGeneratorInfo::class.java)!!.obj
                )
            }
        }
    }

    private val args: EditCustomOnlineGeneratorDialogArgs by navArgs()

    override val title: Int
        get() = R.string.edit_generator
    override val info: OnlineGeneratorInfo
        get() = args.info.obj

    override fun addQueryParamDirections(): NavDirections {
        return EditCustomOnlineGeneratorDialogDirections.actionNavigationEditCustomOnlineGeneratorToNavigationAddQueryParam()
    }

    override fun editQueryParamDirections(param: Pair<String, String>): NavDirections {
        return EditCustomOnlineGeneratorDialogDirections.actionNavigationEditCustomOnlineGeneratorToNavigationEditQueryParam(param.first, param.second)
    }

    override fun testGeneratorDirections(info: OnlineGeneratorInfo): NavDirections {
        return EditCustomOnlineGeneratorDialogDirections.actionNavigationEditCustomOnlineGeneratorToNavigationTestCustomOnlineGenerator(
            ParcelableOnlineGeneratorInfo(info)
        )
    }

    override fun generatorPreconditionsDirections(): NavDirections {
        return EditCustomOnlineGeneratorDialogDirections.actionNavigationEditCustomOnlineGeneratorToNavigationCustomOnlineGeneratorPreconditions()
    }

    override fun onGeneratorCreated(info: OnlineGeneratorInfo) {
        requireActivity().supportFragmentManager.setFragmentResult(
            EDIT_GENERATOR_REQUEST_KEY,
            Bundle().apply {
                putParcelable(OLD_INFO_KEY, args.info)
                putParcelable(NEW_INFO_KEY, ParcelableOnlineGeneratorInfo(info))
            }
        )
    }

}