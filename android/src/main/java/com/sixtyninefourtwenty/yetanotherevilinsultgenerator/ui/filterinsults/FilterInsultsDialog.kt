package com.sixtyninefourtwenty.yetanotherevilinsultgenerator.ui.filterinsults

import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import androidx.fragment.app.activityViewModels
import com.sixtyninefourtwenty.bottomsheetalertdialog.BottomSheetAlertDialogFragmentViewBuilder
import com.sixtyninefourtwenty.bottomsheetalertdialog.DialogButtonProperties
import com.sixtyninefourtwenty.bottomsheetalertdialog.misc.ViewBindingBottomSheetAlertDialogFragment
import com.sixtyninefourtwenty.bottomsheetalertdialog.misc.createBottomSheetAlertDialog
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.R
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.databinding.DialogFilterInsultsBinding
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.Language
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.sortandfilter.InsultFilterOptions
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.sortandfilter.TextFilter
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.ui.offline.OfflineViewModel
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.utils.getInput
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.utils.isInputEmpty
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.utils.languageLabelResId

class FilterInsultsDialog : ViewBindingBottomSheetAlertDialogFragment<DialogFilterInsultsBinding>(DialogFilterInsultsBinding::inflate) {

    private val offlineViewModel: OfflineViewModel by activityViewModels { OfflineViewModel.Factory }

    override fun createDialog(binding: DialogFilterInsultsBinding): BottomSheetAlertDialogFragmentViewBuilder {
        return createBottomSheetAlertDialog(
            view = binding.root,
            titleText = getString(R.string.filter_insults),
            positiveButtonProperties = DialogButtonProperties(
                text = getString(android.R.string.ok),
                listener = {
                    offlineViewModel.insultFilterOptions = InsultFilterOptions(
                        language = binding.languagePicker.selectedItemPosition.takeIf { it != 0 }?.let { Language.entries[it - 1] },
                        textFilter = if (binding.searchInsultContentsInput.isInputEmpty()) null else TextFilter(
                            binding.searchInsultContentsInput.getInput(), TextFilter.Mode.CONTAINS
                        )
                    )
                }
            ),
            neutralButtonProperties = DialogButtonProperties(
                text = getString(R.string.reset),
                listener = { offlineViewModel.insultFilterOptions = null }
            ),
            negativeButtonProperties = DialogButtonProperties(text = getString(android.R.string.cancel))
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val binding = requireBinding()
        binding.languagePicker.adapter = ArrayAdapter(
            requireContext(),
            com.sixtyninefourtwenty.theming.R.layout.spinner_item_m3,
            (intArrayOf(R.string.none) + Language.entries.map { it.languageLabelResId }).map(::getString)
        )
        val existingFilterOptions = offlineViewModel.insultFilterOptions
        if (existingFilterOptions != null) {
            binding.languagePicker.setSelection(existingFilterOptions.language?.ordinal?.plus(1) ?: 0)
            binding.searchInsultContentsInput.setText(existingFilterOptions.textFilter?.textToSearch ?: "")
        }
    }
}