package com.sixtyninefourtwenty.yetanotherevilinsultgenerator.ui.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.databinding.ListItemOnlineGeneratorBinding
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.datasource.InsultsRemoteDataSource
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.dto.OfflineGeneratorInfo
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.dto.OnlineGeneratorInfo

class GeneratorAdapter(
    initialSelectedId: String,
    private val onItemClick: (InsultsRemoteDataSource, GeneratorAdapter) -> Unit,
    private val onMenuClick: ((InsultsRemoteDataSource, View) -> Unit)?
) : ListAdapter<InsultsRemoteDataSource, GeneratorAdapter.ViewHolder>(DIFFER) {

    companion object {
        private val DIFFER = object : DiffUtil.ItemCallback<InsultsRemoteDataSource>() {
            override fun areItemsTheSame(oldItem: InsultsRemoteDataSource, newItem: InsultsRemoteDataSource): Boolean {
                return oldItem.id == newItem.id
            }

            @SuppressLint("DiffUtilEquals")
            override fun areContentsTheSame(oldItem: InsultsRemoteDataSource, newItem: InsultsRemoteDataSource): Boolean {
                if (oldItem.getDisplayName() != newItem.getDisplayName()) {
                    return false
                }
                val oldItemInfo = oldItem.getInfo()
                val newItemInfo = newItem.getInfo()
                return if (oldItemInfo is OnlineGeneratorInfo && newItemInfo is OnlineGeneratorInfo) {
                    oldItemInfo.baseUrl == newItemInfo.baseUrl
                } else if (oldItemInfo is OfflineGeneratorInfo && newItemInfo is OfflineGeneratorInfo) {
                    oldItemInfo.insults == newItemInfo.insults
                } else {
                    false
                }
            }
        }
    }

    var selectedId: String = initialSelectedId
        set(value) {
            if (field != value) {
                val oldValue = field
                field = value
                val previouslyCheckedItemIndex = currentList.indexOfFirst { it.id == oldValue }
                if (previouslyCheckedItemIndex >= 0) {
                    notifyItemChanged(previouslyCheckedItemIndex)
                }
                val newlyCheckedItemIndex = currentList.indexOfFirst { it.id == value }
                if (newlyCheckedItemIndex >= 0) {
                    notifyItemChanged(newlyCheckedItemIndex)
                }
            }
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ListItemOnlineGeneratorBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            ),
            { onItemClick(getItem(it), this) },
            onMenuClick?.let { click -> { pos, v -> click(getItem(pos), v) } }
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item, item.id == selectedId)
    }

    class ViewHolder(
        private val binding: ListItemOnlineGeneratorBinding,
        onItemClick: (Int) -> Unit,
        onMenuClick: ((Int, View) -> Unit)?
    ) : RecyclerView.ViewHolder(binding.root) {
        init {
            binding.root.setOnClickListener { onItemClick(absoluteAdapterPosition) }
            if (onMenuClick != null) {
                binding.menu.setOnClickListener { onMenuClick(bindingAdapterPosition, it) }
            } else {
                binding.menu.visibility = View.GONE
            }
        }

        fun bind(obj: InsultsRemoteDataSource, isSelected: Boolean) {
            binding.checkmark.visibility = if (isSelected) View.VISIBLE else View.INVISIBLE
            binding.displayName.text = obj.getDisplayName()
            when (val info = obj.getInfo()) {
                is OnlineGeneratorInfo -> {
                    binding.baseUrl.text = info.baseUrl
                }
                is OfflineGeneratorInfo -> {
                    binding.baseUrl.text = null
                }
            }
        }
    }

}