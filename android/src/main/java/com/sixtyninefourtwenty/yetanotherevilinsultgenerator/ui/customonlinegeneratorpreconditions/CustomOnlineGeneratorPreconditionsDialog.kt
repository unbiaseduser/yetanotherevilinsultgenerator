package com.sixtyninefourtwenty.yetanotherevilinsultgenerator.ui.customonlinegeneratorpreconditions

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.core.net.toUri
import com.sixtyninefourtwenty.bottomsheetalertdialog.BottomSheetAlertDialogFragmentViewBuilder
import com.sixtyninefourtwenty.bottomsheetalertdialog.misc.ViewBindingBottomSheetAlertDialogFragment
import com.sixtyninefourtwenty.bottomsheetalertdialog.misc.createBottomSheetAlertDialog
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.R
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.databinding.DialogCustomOnlineGeneratorPreconditionsBinding
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.utils.startActivitySafely

class CustomOnlineGeneratorPreconditionsDialog : ViewBindingBottomSheetAlertDialogFragment<DialogCustomOnlineGeneratorPreconditionsBinding>(DialogCustomOnlineGeneratorPreconditionsBinding::inflate) {

    override fun createDialog(binding: DialogCustomOnlineGeneratorPreconditionsBinding): BottomSheetAlertDialogFragmentViewBuilder {
        return createBottomSheetAlertDialog(
            view = binding.root,
            titleText = getString(R.string.preconditions)
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val binding = requireBinding()
        with(binding) {
            openExampleUrl.setOnClickListener {
                startActivitySafely(Intent(Intent.ACTION_VIEW).setData("https://evilinsult.com/generate_insult.php?lang=en&type=json".toUri())) {
                    Toast.makeText(requireContext(), R.string.no_browser_app_installed_error, Toast.LENGTH_SHORT).show()
                }
            }
            openLanguageCodeUrl.setOnClickListener {
                startActivitySafely(Intent(Intent.ACTION_VIEW).setData("https://github.com/EvilInsultGenerator/android-app/blob/development/app/src/main/kotlin/com/evilinsult/viewmodels/Language.kt".toUri())) {
                    Toast.makeText(requireContext(), R.string.no_browser_app_installed_error, Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

}