package com.sixtyninefourtwenty.yetanotherevilinsultgenerator.ui.addeditqueryparam

import android.os.Bundle
import android.view.View
import androidx.annotation.StringRes
import com.sixtyninefourtwenty.bottomsheetalertdialog.BottomSheetAlertDialogFragmentViewBuilder
import com.sixtyninefourtwenty.bottomsheetalertdialog.DialogButtonProperties
import com.sixtyninefourtwenty.bottomsheetalertdialog.misc.ViewBindingBottomSheetAlertDialogFragment
import com.sixtyninefourtwenty.bottomsheetalertdialog.misc.createBottomSheetAlertDialog
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.R
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.databinding.DialogAddEditQueryParamBinding
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.utils.getInput
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.utils.isInputEmpty

abstract class AbstractAddEditQueryParamDialog : ViewBindingBottomSheetAlertDialogFragment<DialogAddEditQueryParamBinding>(DialogAddEditQueryParamBinding::inflate) {

    companion object {
        private const val PARAM_NAME_KEY = "param_name"
        private const val PARAM_VALUE_KEY = "param_value"
    }

    @get:StringRes
    protected abstract val title: Int
    protected abstract val param: Pair<String, String>?

    protected abstract fun onParamCreated(name: String, value: String)

    override fun createDialog(binding: DialogAddEditQueryParamBinding): BottomSheetAlertDialogFragmentViewBuilder {
        return createBottomSheetAlertDialog(
            view = binding.root,
            titleText = getString(title),
            positiveButtonProperties = DialogButtonProperties(
                text = getString(android.R.string.ok),
                listener = {
                    binding.paramNameLayout.error = null
                    binding.paramValueLayout.error = null
                    var isValidationSuccess = true
                    if (binding.paramName.isInputEmpty()) {
                        binding.paramNameLayout.error = getString(R.string.parameter_name_blank_error)
                        isValidationSuccess = false
                    }
                    if (binding.paramValue.isInputEmpty()) {
                        binding.paramValueLayout.error = getString(R.string.parameter_value_blank_error)
                        isValidationSuccess = false
                    }
                    if (isValidationSuccess) {
                        onParamCreated(
                            name = binding.paramName.getInput(),
                            value = binding.paramValue.getInput()
                        )
                        dismiss()
                    }
                },
                dismissAfterClick = false
            ),
            negativeButtonProperties = DialogButtonProperties(
                text = getString(android.R.string.cancel)
            )
        )
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        with(outState) {
            putString(PARAM_NAME_KEY, requireBinding().paramName.getInput())
            putString(PARAM_VALUE_KEY, requireBinding().paramValue.getInput())
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val binding = requireBinding()
        if (savedInstanceState == null) {
            val p = param
            if (p != null) {
                with(binding) {
                    paramName.setText(p.first)
                    paramValue.setText(p.second)
                }
            }
        } else {
            with(binding) {
                paramName.setText(savedInstanceState.getString(PARAM_NAME_KEY)!!)
                paramValue.setText(savedInstanceState.getString(PARAM_VALUE_KEY)!!)
            }
        }
    }

}