package com.sixtyninefourtwenty.yetanotherevilinsultgenerator.ui.addeditcustomonlinegenerator

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.StringRes
import androidx.core.os.BundleCompat
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.sixtyninefourtwenty.bottomsheetalertdialog.BottomSheetAlertDialogFragmentViewBuilder
import com.sixtyninefourtwenty.bottomsheetalertdialog.DialogButtonProperties
import com.sixtyninefourtwenty.bottomsheetalertdialog.misc.ViewBindingBottomSheetAlertDialogFragment
import com.sixtyninefourtwenty.bottomsheetalertdialog.misc.createBottomSheetAlertDialog
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.R
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.data.model.ParcelableOnlineGeneratorInfo
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.databinding.DialogAddEditCustomOnlineGeneratorBinding
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.databinding.ListItemQueryParamBinding
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.dto.OnlineGeneratorInfo
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.utils.randomUniqueString
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.ui.addeditqueryparam.AddQueryParamDialog
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.ui.addeditqueryparam.EditQueryParamDialog
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.ui.testcustomonlinegenerator.TestCustomOnlineGeneratorDialog
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.utils.getInput
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.utils.isInputEmpty
import io.realm.kotlin.ext.realmDictionaryOf
import kotlin.properties.Delegates

abstract class AbstractAddEditCustomOnlineGeneratorDialog : ViewBindingBottomSheetAlertDialogFragment<DialogAddEditCustomOnlineGeneratorBinding>(DialogAddEditCustomOnlineGeneratorBinding::inflate) {

    companion object {
        private const val TEST_STATE_KEY = "test_state"
        private const val INFO_KEY = "info"
    }

    @get:StringRes
    protected abstract val title: Int
    protected abstract val info: OnlineGeneratorInfo?

    protected abstract fun addQueryParamDirections(): NavDirections
    protected abstract fun editQueryParamDirections(param: Pair<String, String>): NavDirections
    protected abstract fun testGeneratorDirections(info: OnlineGeneratorInfo): NavDirections
    protected abstract fun generatorPreconditionsDirections(): NavDirections
    protected abstract fun onGeneratorCreated(info: OnlineGeneratorInfo)

    private lateinit var adapter: QueryParamAdapter

    private var testState: TestCustomOnlineGeneratorDialog.State by Delegates.vetoable(
        TestCustomOnlineGeneratorDialog.State.UNTESTED
    ) { _, oldValue, newValue ->
        if (oldValue == TestCustomOnlineGeneratorDialog.State.UNTESTED) {
            true
        } else {
            newValue != TestCustomOnlineGeneratorDialog.State.UNTESTED
        }
    }

    private fun validateInput(binding: DialogAddEditCustomOnlineGeneratorBinding): Boolean {
        binding.baseUrlInputLayout.error = null
        binding.displayNameInputLayout.error = null
        var isValidationSuccess = true
        if (binding.baseUrlInput.isInputEmpty()) {
            binding.baseUrlInputLayout.error = getString(R.string.base_url_blank_error)
            isValidationSuccess = false
        }
        if (binding.displayNameInput.isInputEmpty()) {
            binding.displayNameInputLayout.error = getString(R.string.display_name_blank_error)
            isValidationSuccess = false
        }
        return isValidationSuccess
    }

    private fun createGeneratorInfo(binding: DialogAddEditCustomOnlineGeneratorBinding) = OnlineGeneratorInfo(
        id = info?.id ?: randomUniqueString(),
        displayName = binding.displayNameInput.getInput(),
        baseUrl = binding.baseUrlInput.getInput(),
        languageParameterName = binding.languageParameterNameInput.getInput().takeIf { it.isNotBlank() },
        extraQueryParams = realmDictionaryOf(adapter.currentList)
    )

    override fun createDialog(binding: DialogAddEditCustomOnlineGeneratorBinding): BottomSheetAlertDialogFragmentViewBuilder {
        return createBottomSheetAlertDialog(
            view = binding.root,
            titleText = getString(title),
            positiveButtonProperties = DialogButtonProperties(
                text = getString(android.R.string.ok),
                listener = {
                    if (validateInput(binding)) {
                        when (testState) {
                            TestCustomOnlineGeneratorDialog.State.FAILED -> {
                                MaterialAlertDialogBuilder(requireContext())
                                    .setTitle(R.string.generator_test_failed)
                                    .setMessage(R.string.generator_test_failed_add_prompt)
                                    .setPositiveButton(R.string.add_anyway) { _, _ ->
                                        onGeneratorCreated(
                                            createGeneratorInfo(binding)
                                        )
                                        dismiss()
                                    }
                                    .setNegativeButton(android.R.string.cancel, null)
                                    .show()
                            }
                            TestCustomOnlineGeneratorDialog.State.UNTESTED -> {
                                MaterialAlertDialogBuilder(requireContext())
                                    .setTitle(R.string.generator_untested)
                                    .setMessage(R.string.generator_untested_add_prompt)
                                    .setPositiveButton(R.string.add_anyway) { _, _ ->
                                        onGeneratorCreated(
                                            createGeneratorInfo(binding)
                                        )
                                        dismiss()
                                    }
                                    .setNegativeButton(android.R.string.cancel, null)
                                    .show()
                            }
                            else -> {
                                onGeneratorCreated(
                                    createGeneratorInfo(binding)
                                )
                                dismiss()
                            }
                        }
                    }
                },
                dismissAfterClick = false
            ),
            negativeButtonProperties = DialogButtonProperties(
                text = getString(android.R.string.cancel)
            ),
            neutralButtonProperties = DialogButtonProperties(
                text = getString(R.string.test),
                listener = {
                    if (validateInput(binding)) {
                        findNavController().navigate(testGeneratorDirections(createGeneratorInfo(binding)))
                    }
                },
                dismissAfterClick = false
            )
        )
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        with(outState) {
            putString(TEST_STATE_KEY, testState.name)
            putParcelable(INFO_KEY, ParcelableOnlineGeneratorInfo(createGeneratorInfo(requireBinding())))
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val binding = requireBinding()
        binding.preconditions.setOnClickListener {
            findNavController().navigate(generatorPreconditionsDirections())
        }
        binding.addExtraQueryParameter.setOnClickListener {
            findNavController().navigate(addQueryParamDirections())
        }
        adapter = QueryParamAdapter(
            onDeleteClick = { a, item ->
                MaterialAlertDialogBuilder(requireContext())
                    .setTitle(R.string.delete)
                    .setMessage(R.string.delete_query_parameter_prompt)
                    .setPositiveButton(android.R.string.ok) { _, _ ->
                        a.submitList(a.currentList - item)
                    }
                    .setNegativeButton(android.R.string.cancel, null)
                    .show()
            },
            onEditClick = {
                findNavController().navigate(editQueryParamDirections(it))
            }
        )
        with(binding.extraQueryParameters) {
            layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
            adapter = this@AbstractAddEditCustomOnlineGeneratorDialog.adapter
        }

        if (savedInstanceState != null) {
            testState = TestCustomOnlineGeneratorDialog.State.valueOf(savedInstanceState.getString(TEST_STATE_KEY)!!)
        }
        val info = if (savedInstanceState == null) {
            this.info
        } else {
            BundleCompat.getParcelable(savedInstanceState, INFO_KEY, ParcelableOnlineGeneratorInfo::class.java)!!.obj
        }
        if (info != null) {
            with(binding) {
                baseUrlInput.setText(info.baseUrl)
                displayNameInput.setText(info.displayName)
                languageParameterNameInput.setText(info.languageParameterName)
                adapter.submitList(info.extraQueryParams.toList())
            }
        }

        AddQueryParamDialog.registerCallback(
            requireActivity(),
            viewLifecycleOwner
        ) { name, value ->
            adapter.submitList(adapter.currentList + (name to value))
        }

        EditQueryParamDialog.registerCallback(
            requireActivity(),
            viewLifecycleOwner
        ) { oldName, newName, oldValue, newValue ->
            adapter.submitList(ArrayList(adapter.currentList).apply {
                replaceAll { param ->
                    if (param.first == oldName && param.second == oldValue) {
                        newName to newValue
                    } else {
                        param
                    }
                }
            })
        }

        TestCustomOnlineGeneratorDialog.registerCallback(
            requireActivity(),
            viewLifecycleOwner
        ) { state ->
            this.testState = state
        }

    }

    private class QueryParamAdapter(
        private val onDeleteClick: (QueryParamAdapter, Pair<String, String>) -> Unit,
        private val onEditClick: (Pair<String, String>) -> Unit
    ) : ListAdapter<Pair<String, String>, QueryParamAdapter.ViewHolder>(DIFFER) {

        companion object {
            val DIFFER = object : DiffUtil.ItemCallback<Pair<String, String>>() {
                override fun areItemsTheSame(oldItem: Pair<String, String>, newItem: Pair<String, String>): Boolean {
                    return oldItem == newItem
                }

                override fun areContentsTheSame(oldItem: Pair<String, String>, newItem: Pair<String, String>): Boolean {
                    return areItemsTheSame(oldItem, newItem)
                }
            }
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            return ViewHolder(
                ListItemQueryParamBinding.inflate(LayoutInflater.from(parent.context), parent, false),
                { onDeleteClick(this, getItem(it)) },
                { onEditClick(getItem(it)) }
            )
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.bind(getItem(position))
        }

        private class ViewHolder(
            private val binding: ListItemQueryParamBinding,
            onDeleteClick: (Int) -> Unit,
            onEditClick: (Int) -> Unit
        ) : RecyclerView.ViewHolder(binding.root) {

            init {
                binding.delete.setOnClickListener {
                    onDeleteClick(bindingAdapterPosition)
                }
                binding.edit.setOnClickListener {
                    onEditClick(bindingAdapterPosition)
                }
            }

            fun bind(item: Pair<String, String>) {
                with(binding) {
                    paramName.setText(item.first)
                    paramValue.setText(item.second)
                }
            }

        }

    }

}