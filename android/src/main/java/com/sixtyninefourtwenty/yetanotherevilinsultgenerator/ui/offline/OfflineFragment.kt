package com.sixtyninefourtwenty.yetanotherevilinsultgenerator.ui.offline

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.view.ActionMode
import androidx.core.view.MenuProvider
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.selection.SelectionPredicates
import androidx.recyclerview.selection.SelectionTracker
import androidx.recyclerview.selection.SelectionTracker.SelectionObserver
import androidx.recyclerview.selection.StorageStrategy
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.kennyc.view.MultiStateView
import com.sixtyninefourtwenty.basefragments.ViewBindingFragment
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.R
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.data.model.ParcelableInsultDto
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.databinding.FragmentOfflineBinding
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.ui.adapter.InsultAdapter
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.utils.DisposableHandleContainer
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.utils.addMenuProvider
import kotlinx.coroutines.launch

class OfflineFragment : ViewBindingFragment<FragmentOfflineBinding>(FragmentOfflineBinding::inflate) {

    private lateinit var disposableHandleContainer: DisposableHandleContainer
    private val offlineViewModel: OfflineViewModel by activityViewModels { OfflineViewModel.Factory }
    private val insultsAdapter = InsultAdapter {
        findNavController().navigate(OfflineFragmentDirections.actionNavigationOfflineToNavigationInsultInfo(
            ParcelableInsultDto(it)
        ))
    }
    private lateinit var selectionTracker: SelectionTracker<Long>
    private var actionMode: ActionMode? = null

    private val menuProvider = object : MenuProvider {
        override fun onCreateMenu(menu: Menu, menuInflater: MenuInflater) {
            menuInflater.inflate(R.menu.offline_fragment_menu, menu)
        }

        override fun onMenuItemSelected(menuItem: MenuItem): Boolean {
            return when (menuItem.itemId) {
                R.id.filter_insults -> {
                    findNavController().navigate(OfflineFragmentDirections.actionNavigationOfflineToNavigationFilterInsults())
                    true
                }
                R.id.sort_insults -> {
                    findNavController().navigate(OfflineFragmentDirections.actionNavigationOfflineToNavigationSortInsults())
                    true
                }
                else -> false
            }
        }

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val binding = requireBinding()
        disposableHandleContainer = DisposableHandleContainer(viewLifecycleOwner)
        addMenuProvider(menuProvider)
        with(binding.offlineInsults) {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = insultsAdapter
        }
        selectionTracker = SelectionTracker.Builder(
            "selected_insults",
            binding.offlineInsults,
            insultsAdapter.KeyProvider(),
            InsultAdapter.DetailsLookup(binding.offlineInsults),
            StorageStrategy.createLongStorage()
        ).withSelectionPredicate(SelectionPredicates.createSelectAnything()).build().also { tracker ->
            tracker.addObserver(object : SelectionObserver<Long>() {
                override fun onSelectionChanged() {
                    val selection = tracker.selection
                    if (selection.isEmpty) {
                        actionMode?.finish()
                    } else {
                        val a = actionMode
                        if (a == null) {
                            actionMode = (requireActivity() as AppCompatActivity).startSupportActionMode(object : ActionMode.Callback {

                                override fun onCreateActionMode(mode: ActionMode, menu: Menu): Boolean {
                                    mode.menuInflater.inflate(R.menu.insults_selection_action_mode_menu, menu)
                                    return true
                                }

                                override fun onPrepareActionMode(mode: ActionMode, menu: Menu): Boolean {
                                    mode.title = resources.getQuantityString(R.plurals.insults, selection.size(), selection.size())
                                    return true
                                }

                                override fun onActionItemClicked(mode: ActionMode, item: MenuItem): Boolean {
                                    when (item.itemId) {
                                        R.id.delete -> {
                                            val insultsToDelete = insultsAdapter.getSelectedItems().filter { !it.isFavorite }
                                            if (insultsToDelete.isEmpty()) {
                                                Toast.makeText(requireContext(), R.string.all_insults_selected_are_favorites, Toast.LENGTH_SHORT).show()
                                            } else {
                                                MaterialAlertDialogBuilder(requireContext())
                                                    .setTitle(R.string.delete)
                                                    .setMessage(getString(R.string.delete_confirmation, resources.getQuantityString(R.plurals.insults, insultsToDelete.size, insultsToDelete.size)))
                                                    .setPositiveButton(android.R.string.ok) { _, _ ->
                                                        val job = offlineViewModel.deleteLocalInsults(insultsToDelete)
                                                        disposableHandleContainer.add(job.invokeOnCompletion {
                                                            viewLifecycleOwner.lifecycleScope.launch {
                                                                Toast.makeText(requireContext(), getString(R.string.deleted, resources.getQuantityString(R.plurals.insults, insultsToDelete.size, insultsToDelete.size)), Toast.LENGTH_SHORT).show()
                                                            }
                                                        })
                                                    }
                                                    .show()
                                            }

                                            return true
                                        }
                                    }
                                    return false
                                }

                                override fun onDestroyActionMode(mode: ActionMode) {
                                    tracker.clearSelection()
                                    actionMode = null
                                }

                            })
                        } else {
                            a.invalidate()
                        }
                    }
                }
            })
            tracker.onRestoreInstanceState(savedInstanceState)
            insultsAdapter.selectionTracker = tracker
        }

        offlineViewModel.insultsAfterSearchAndFilter.observe(viewLifecycleOwner) {
            binding.stateView.viewState = if (it.isEmpty()) MultiStateView.ViewState.EMPTY else MultiStateView.ViewState.CONTENT
            insultsAdapter.submitList(it)
        }

    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        if (::selectionTracker.isInitialized) {
            selectionTracker.onSaveInstanceState(outState)
        }
    }

}