package com.sixtyninefourtwenty.yetanotherevilinsultgenerator.ui.addrating

import android.os.Bundle
import androidx.core.os.BundleCompat
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.LifecycleOwner
import com.sixtyninefourtwenty.bottomsheetalertdialog.BottomSheetAlertDialogFragmentViewBuilder
import com.sixtyninefourtwenty.bottomsheetalertdialog.DialogButtonProperties
import com.sixtyninefourtwenty.bottomsheetalertdialog.misc.ViewBindingBottomSheetAlertDialogFragment
import com.sixtyninefourtwenty.bottomsheetalertdialog.misc.createBottomSheetAlertDialog
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.R
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.data.model.ParcelableInsultRatingDto
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.databinding.DialogAddEditRatingBinding
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.dto.InsultRatingDto

class AddRatingDialog : ViewBindingBottomSheetAlertDialogFragment<DialogAddEditRatingBinding>(DialogAddEditRatingBinding::inflate) {

    companion object {
        private const val ADD_RATING_REQUEST_KEY = "add_rating"
        private const val RATING_KEY = "rating"
        fun registerCallback(
            activity: FragmentActivity,
            lifecycleOwner: LifecycleOwner,
            callback: (InsultRatingDto) -> Unit
        ) {
            activity.supportFragmentManager.setFragmentResultListener(
                ADD_RATING_REQUEST_KEY,
                lifecycleOwner
            ) { _, result ->
                callback(
                    BundleCompat.getParcelable(result, RATING_KEY, ParcelableInsultRatingDto::class.java)!!.obj
                )
            }
        }
    }

    override fun createDialog(binding: DialogAddEditRatingBinding): BottomSheetAlertDialogFragmentViewBuilder {
        return createBottomSheetAlertDialog(
            view = binding.root,
            titleText = getString(R.string.add_rating),
            positiveButtonProperties = DialogButtonProperties(
                text = getString(android.R.string.ok),
                listener = {
                    requireActivity().supportFragmentManager.setFragmentResult(
                        ADD_RATING_REQUEST_KEY,
                        Bundle().apply {
                            putParcelable(RATING_KEY, ParcelableInsultRatingDto(InsultRatingDto(
                                rating = binding.ratingSlider.value,
                                explanation = binding.explanationInput.text?.toString()?.takeIf { it.isNotBlank() }
                            )))
                        }
                    )
                }
            )
        )
    }

}