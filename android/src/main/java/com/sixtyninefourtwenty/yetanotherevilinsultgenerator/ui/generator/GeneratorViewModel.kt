package com.sixtyninefourtwenty.yetanotherevilinsultgenerator.ui.generator

import android.net.ConnectivityManager
import android.net.ConnectivityManager.NetworkCallback
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkRequest
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProvider.AndroidViewModelFactory.Companion.APPLICATION_KEY
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.initializer
import androidx.lifecycle.viewmodel.viewModelFactory
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.MyApplication
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.ViewState
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.Language
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.dto.InsultDto
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.repository.InsultsRepository
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.utils.asExceptionOrThrow
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.launch

class GeneratorViewModel(
    private val insultsRepository: InsultsRepository
) : ViewModel() {

    private val mutableViewState = MutableStateFlow<ViewState<InsultDto>>(ViewState.Empty)
    val viewState = mutableViewState.asStateFlow()

    fun generateRemoteInsult(
        language: Language,
        id: String
    ) = viewModelScope.launch {
        mutableViewState.value = ViewState.Loading
        runCatching {
            insultsRepository.generateRemoteInsult(id, language)
        }.onFailure {
            mutableViewState.value = ViewState.Error(it.asExceptionOrThrow())
        }.onSuccess {
            upsertInsult(it)
            mutableViewState.value = ViewState.Content(it)
        }
    }

    fun upsertInsult(insult: InsultDto) = viewModelScope.launch {
        insultsRepository.upsertInsult(insult)
    }

    fun checkNetworkAvailability(
        connectivityManager: ConnectivityManager
    ): Flow<Boolean> = callbackFlow {

        // Network callback isn't called when the app initially doesn't have network connection.
        trySend(false)

        val networkCallback = object : NetworkCallback() {
            override fun onAvailable(network: Network) {
                trySend(true)
            }

            override fun onLost(network: Network) {
                trySend(false)
            }
        }

        connectivityManager.registerNetworkCallback(
            NetworkRequest.Builder().addCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET).build(),
            networkCallback
        )

        awaitClose { connectivityManager.unregisterNetworkCallback(networkCallback) }
    }

    companion object {
        val Factory: ViewModelProvider.Factory = viewModelFactory {
            initializer {
                val app = get(APPLICATION_KEY) as MyApplication
                GeneratorViewModel(app.insultsRepository)
            }
        }
    }
}