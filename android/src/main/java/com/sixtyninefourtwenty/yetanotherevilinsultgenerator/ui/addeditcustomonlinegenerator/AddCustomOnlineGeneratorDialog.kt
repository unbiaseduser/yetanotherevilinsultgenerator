package com.sixtyninefourtwenty.yetanotherevilinsultgenerator.ui.addeditcustomonlinegenerator

import android.os.Bundle
import androidx.core.os.BundleCompat
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.LifecycleOwner
import androidx.navigation.NavDirections
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.R
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.data.model.ParcelableOnlineGeneratorInfo
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.dto.OnlineGeneratorInfo

class AddCustomOnlineGeneratorDialog : AbstractAddEditCustomOnlineGeneratorDialog() {

    companion object {
        private const val ADD_GENERATOR_REQUEST_KEY = "add_generator"
        private const val INFO_KEY = "info"
        fun registerCallback(
            activity: FragmentActivity,
            lifecycleOwner: LifecycleOwner,
            callback: (OnlineGeneratorInfo) -> Unit
        ) {
            activity.supportFragmentManager.setFragmentResultListener(
                ADD_GENERATOR_REQUEST_KEY,
                lifecycleOwner
            ) { _, result ->
                callback(
                    BundleCompat.getParcelable(result, INFO_KEY, ParcelableOnlineGeneratorInfo::class.java)!!.obj
                )
            }
        }
    }

    override val title: Int
        get() = R.string.add_generator
    override val info: OnlineGeneratorInfo? = null

    override fun addQueryParamDirections(): NavDirections {
        return AddCustomOnlineGeneratorDialogDirections.actionNavigationAddCustomOnlineGeneratorToNavigationAddQueryParam()
    }

    override fun editQueryParamDirections(param: Pair<String, String>): NavDirections {
        return AddCustomOnlineGeneratorDialogDirections.actionNavigationAddCustomOnlineGeneratorToNavigationEditQueryParam(param.first, param.second)
    }

    override fun testGeneratorDirections(info: OnlineGeneratorInfo): NavDirections {
        return AddCustomOnlineGeneratorDialogDirections.actionNavigationAddCustomOnlineGeneratorToNavigationTestCustomOnlineGenerator(
            ParcelableOnlineGeneratorInfo(info)
        )
    }

    override fun generatorPreconditionsDirections(): NavDirections {
        return AddCustomOnlineGeneratorDialogDirections.actionNavigationAddCustomOnlineGeneratorToNavigationCustomOnlineGeneratorPreconditions()
    }

    override fun onGeneratorCreated(info: OnlineGeneratorInfo) {
        requireActivity().supportFragmentManager.setFragmentResult(
            ADD_GENERATOR_REQUEST_KEY,
            Bundle().apply {
                putParcelable(INFO_KEY, ParcelableOnlineGeneratorInfo(info))
            }
        )
    }

}