package com.sixtyninefourtwenty.yetanotherevilinsultgenerator.ui.pickofficialgenerator

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.sixtyninefourtwenty.bottomsheetalertdialog.BottomSheetAlertDialogFragmentViewBuilder
import com.sixtyninefourtwenty.bottomsheetalertdialog.misc.ViewBottomSheetAlertDialogFragment
import com.sixtyninefourtwenty.bottomsheetalertdialog.misc.createBottomSheetAlertDialog
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.R
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.ui.adapter.GeneratorAdapter
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.utils.myApplication

class PickOfficialGeneratorDialog : ViewBottomSheetAlertDialogFragment<RecyclerView>(::RecyclerView) {

    override fun createDialog(view: RecyclerView): BottomSheetAlertDialogFragmentViewBuilder {
        return createBottomSheetAlertDialog(
            view = view,
            titleText = getString(R.string.pick_generator)
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val prefs = requireContext().myApplication.preferencesRepository
        val generatorAdapter = GeneratorAdapter(
            initialSelectedId = prefs.generatorId,
            onItemClick = { ds, a ->
                prefs.generatorId = ds.id
                a.selectedId = ds.id
            },
            onMenuClick = null
        ).apply {
            submitList(requireContext().myApplication.insultsRepository.officialRemoteDataSources)
        }
        with(requireTypedView()) {
            layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
            adapter = generatorAdapter
        }

    }

}