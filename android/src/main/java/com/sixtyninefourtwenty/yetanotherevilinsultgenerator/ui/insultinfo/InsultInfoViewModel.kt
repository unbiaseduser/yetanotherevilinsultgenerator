package com.sixtyninefourtwenty.yetanotherevilinsultgenerator.ui.insultinfo

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProvider.AndroidViewModelFactory.Companion.APPLICATION_KEY
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.initializer
import androidx.lifecycle.viewmodel.viewModelFactory
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.MyApplication
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.dto.InsultDto
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.dto.InsultRatingDto
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.repository.InsultsRepository
import kotlinx.coroutines.launch

class InsultInfoViewModel(private val repository: InsultsRepository) : ViewModel() {

    fun updateFavoriteStatus(insult: InsultDto, isFavorite: Boolean) = viewModelScope.launch {
        repository.updateFavoriteStatus(insult, isFavorite)
    }

    fun getFavoriteStatus(insult: InsultDto) = repository.getFavoriteStatus(insult)

    fun getRating(insult: InsultDto) = repository.getRating(insult)

    fun updateRating(insult: InsultDto, rating: InsultRatingDto?) = viewModelScope.launch {
        repository.updateRating(insult, rating)
    }

    fun deleteLocalInsult(insult: InsultDto) = viewModelScope.launch {
        repository.deleteLocalInsult(insult)
    }

    companion object {
        val Factory: ViewModelProvider.Factory = viewModelFactory {
            initializer {
                val repo = (get(APPLICATION_KEY) as MyApplication).insultsRepository
                InsultInfoViewModel(repo)
            }
        }
    }
}