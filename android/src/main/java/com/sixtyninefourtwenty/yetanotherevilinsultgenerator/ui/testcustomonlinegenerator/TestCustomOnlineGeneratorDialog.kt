package com.sixtyninefourtwenty.yetanotherevilinsultgenerator.ui.testcustomonlinegenerator

import android.os.Bundle
import android.view.View
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.navArgs
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.kennyc.view.MultiStateView
import com.sixtyninefourtwenty.bottomsheetalertdialog.BottomSheetAlertDialogFragmentViewBuilder
import com.sixtyninefourtwenty.bottomsheetalertdialog.misc.ViewBindingBottomSheetAlertDialogFragment
import com.sixtyninefourtwenty.bottomsheetalertdialog.misc.createBottomSheetAlertDialog
import com.sixtyninefourtwenty.bottomsheetalertdialog.view.BottomSheetAlertDialogContentView
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.R
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.databinding.DialogTestCustomOnlineGeneratorBinding
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.databinding.DialogTestCustomOnlineGeneratorStateErrorBinding
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.ViewState
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.Language
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.utils.bottomPaddedWithDefaultDimension
import io.ktor.http.URLBuilder
import kotlinx.coroutines.launch

class TestCustomOnlineGeneratorDialog : ViewBindingBottomSheetAlertDialogFragment<DialogTestCustomOnlineGeneratorBinding>(DialogTestCustomOnlineGeneratorBinding::inflate) {

    companion object {
        private const val STATE_REQUEST_KEY = "state"
        private const val STATE_KEY = "s"
        fun registerCallback(
            activity: FragmentActivity,
            lifecycleOwner: LifecycleOwner,
            callback: (State) -> Unit
        ) {
            activity.supportFragmentManager.setFragmentResultListener(
                STATE_REQUEST_KEY,
                lifecycleOwner
            ) { _, result ->
                callback(State.valueOf(result.getString(STATE_KEY)!!))
            }
        }
    }

    enum class State {
        UNTESTED, SUCCESS, FAILED
    }

    private val args: TestCustomOnlineGeneratorDialogArgs by navArgs()
    private val viewModel: TestCustomOnlineGeneratorViewModel by viewModels()

    override fun createDialog(binding: DialogTestCustomOnlineGeneratorBinding): BottomSheetAlertDialogFragmentViewBuilder {
        return createBottomSheetAlertDialog(
            titleText = getString(R.string.test_generator)
        ).setContentView(
            BottomSheetAlertDialogContentView
                .verticallyScrollable(binding)
                .bottomPaddedWithDefaultDimension(requireContext())
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val binding = requireBinding()
        val info = args.info.obj
        val url = URLBuilder(urlString = args.info.obj.baseUrl).apply {
            val languageParameterName = info.languageParameterName
            if (languageParameterName != null) {
                parameters.append(languageParameterName, Language.ENGLISH.languageCode)
            }
            info.extraQueryParams.forEach { (name, value) ->
                parameters.append(name, value)
            }
        }.buildString()
        binding.url.text = url
        binding.test.setOnClickListener {
            viewModel.test(info)
        }

        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.viewStateFlow.collect { viewState ->
                    when (viewState) {
                        is ViewState.Content -> {
                            binding.state.viewState = MultiStateView.ViewState.CONTENT
                            binding.stateContent.insult.setOnClickListener { _ ->
                                MaterialAlertDialogBuilder(requireContext())
                                    .setTitle(R.string.insult)
                                    .setMessage(viewState.data.toString())
                                    .setPositiveButton(android.R.string.ok, null)
                                    .show()
                            }
                        }
                        is ViewState.Error -> {
                            binding.state.viewState = MultiStateView.ViewState.ERROR
                            val errorBinding = DialogTestCustomOnlineGeneratorStateErrorBinding.bind(binding.state.getView(MultiStateView.ViewState.ERROR)!!)
                            errorBinding.viewError.setOnClickListener { _ ->
                                MaterialAlertDialogBuilder(requireContext())
                                    .setTitle(R.string.error)
                                    .setMessage(viewState.exception.stackTraceToString())
                                    .setPositiveButton(android.R.string.ok, null)
                                    .show()
                            }
                        }
                        is ViewState.Loading -> binding.state.viewState = MultiStateView.ViewState.LOADING
                        is ViewState.Empty -> binding.state.viewState = MultiStateView.ViewState.EMPTY
                    }
                }
            }
        }
    }

    override fun onDestroyView(binding: DialogTestCustomOnlineGeneratorBinding) {
        val resultState = when (binding.state.viewState) {
            MultiStateView.ViewState.CONTENT -> State.SUCCESS
            MultiStateView.ViewState.ERROR -> State.FAILED
            MultiStateView.ViewState.LOADING, MultiStateView.ViewState.EMPTY -> State.UNTESTED
        }
        requireActivity().supportFragmentManager.setFragmentResult(
            STATE_REQUEST_KEY,
            Bundle().apply {
                putString(STATE_KEY, resultState.name)
            }
        )
    }

}