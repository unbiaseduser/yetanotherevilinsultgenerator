package com.sixtyninefourtwenty.yetanotherevilinsultgenerator.ui.testcustomonlinegenerator

import android.os.Bundle
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.ViewState
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.Language
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.dto.InsultDto
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.dto.OnlineGeneratorInfo
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.utils.asExceptionOrThrow
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.utils.toBundle
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.utils.toViewState
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch

class TestCustomOnlineGeneratorViewModel(
    private val ssh: SavedStateHandle
) : ViewModel() {

    val viewStateFlow = ssh.getStateFlow(STATE_STATE_KEY, ViewState.Empty.toBundle()).map {
        it.toViewState()
    }

    var viewState: ViewState<InsultDto>
        get() = ssh.get<Bundle>(STATE_STATE_KEY)!!.toViewState()
        set(value) {
            ssh[STATE_STATE_KEY] = value.toBundle()
        }

    fun test(
        info: OnlineGeneratorInfo
    ) = viewModelScope.launch {
        viewState = ViewState.Loading
        runCatching {
            info.toOnlineGenerator().generateRemoteInsult(Language.ENGLISH)
        }.onSuccess {
            viewState = ViewState.Content(it)
        }.onFailure {
            viewState = ViewState.Error(it.asExceptionOrThrow())
        }
    }

    companion object {
        private const val STATE_STATE_KEY = "state"
    }

}