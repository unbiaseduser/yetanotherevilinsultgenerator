package com.sixtyninefourtwenty.yetanotherevilinsultgenerator.ui.insultinfo

import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.sixtyninefourtwenty.bottomsheetalertdialog.BottomSheetAlertDialogFragmentViewBuilder
import com.sixtyninefourtwenty.bottomsheetalertdialog.misc.ViewBindingBottomSheetAlertDialogFragment
import com.sixtyninefourtwenty.bottomsheetalertdialog.misc.createBottomSheetAlertDialog
import com.sixtyninefourtwenty.bottomsheetalertdialog.view.BottomSheetAlertDialogContentView
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.R
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.data.model.ParcelableInsultRatingDto
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.databinding.DialogInsultInfoBinding
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.createInsultJsonFieldFromLocalDateTime
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.dto.InsultDto
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.ui.addrating.AddRatingDialog
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.ui.editrating.EditRatingExplanationDialog
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.ui.editrating.EditRatingScoreDialog
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.utils.DisposableHandleContainer
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.utils.bottomPaddedWithDefaultDimension
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.utils.copyToClipboard
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.utils.languageLabelResId
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.utils.share
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.utils.tint
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch
import java.util.Locale

class InsultInfoDialog : ViewBindingBottomSheetAlertDialogFragment<DialogInsultInfoBinding>(DialogInsultInfoBinding::inflate) {

    private lateinit var disposableHandleContainer: DisposableHandleContainer
    private val insultPropertiesObserverScope = CoroutineScope(Dispatchers.Main.immediate)
    private val args: InsultInfoDialogArgs by navArgs()
    private val insult: InsultDto by lazy { args.insult.obj }
    private val insultInfoViewModel: InsultInfoViewModel by viewModels { InsultInfoViewModel.Factory }
    
    private val addInsultToFavorites = View.OnClickListener {
        MaterialAlertDialogBuilder(requireContext())
            .setTitle(R.string.add_to_favorites)
            .setMessage(R.string.add_insult_to_favorites_prompt)
            .setPositiveButton(android.R.string.ok) { _, _ ->
                val job = insultInfoViewModel.updateFavoriteStatus(insult, true)
                disposableHandleContainer.add(job.invokeOnCompletion {
                    viewLifecycleOwner.lifecycleScope.launch {
                        Toast.makeText(requireContext(), R.string.added_to_favorites, Toast.LENGTH_SHORT).show()
                    }
                })
            }
            .setNegativeButton(android.R.string.cancel, null)
            .show()
    }
    
    private val removeInsultFromFavorites = View.OnClickListener {
        MaterialAlertDialogBuilder(requireContext())
            .setTitle(R.string.delete_from_favorites)
            .setMessage(R.string.remove_insult_from_favorites_prompt)
            .setPositiveButton(android.R.string.ok) { _, _ ->
                val job = insultInfoViewModel.updateFavoriteStatus(insult, false)
                disposableHandleContainer.add(job.invokeOnCompletion {
                    viewLifecycleOwner.lifecycleScope.launch {
                        Toast.makeText(requireContext(), R.string.removed_from_favorites, Toast.LENGTH_SHORT).show()
                    }
                })
            }
            .setNegativeButton(android.R.string.cancel, null)
            .show()
    }

    private val showAddRatingDialog = View.OnClickListener {
        findNavController().navigate(InsultInfoDialogDirections.actionNavigationInsultInfoToNavigationAddRating())
    }

    private val deleteRating = View.OnClickListener {
        MaterialAlertDialogBuilder(requireContext())
            .setTitle(R.string.delete_rating)
            .setMessage(R.string.delete_rating_prompt)
            .setPositiveButton(android.R.string.ok) { _, _ ->
                val job = insultInfoViewModel.updateRating(insult, null)
                disposableHandleContainer.add(job.invokeOnCompletion {
                    viewLifecycleOwner.lifecycleScope.launch {
                        Toast.makeText(requireContext(), R.string.successfully_deleted_rating, Toast.LENGTH_SHORT).show()
                    }
                })
            }
            .setNegativeButton(android.R.string.cancel, null)
            .show()
    }

    override fun createDialog(binding: DialogInsultInfoBinding): BottomSheetAlertDialogFragmentViewBuilder {
        return createBottomSheetAlertDialog(
            titleText = getString(R.string.insult_info)
        ).setContentView(
            BottomSheetAlertDialogContentView
                .verticallyScrollable(binding)
                .bottomPaddedWithDefaultDimension(requireContext())
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val binding = requireBinding()
        disposableHandleContainer = DisposableHandleContainer(viewLifecycleOwner)
        viewLifecycleOwner.lifecycle.addObserver(object : LifecycleEventObserver {
            override fun onStateChanged(source: LifecycleOwner, event: Lifecycle.Event) {
                if (event == Lifecycle.Event.ON_DESTROY) {
                    insultPropertiesObserverScope.cancel()
                }
            }
        })
        binding.delete.setOnClickListener {
            MaterialAlertDialogBuilder(requireContext())
                .setTitle(R.string.delete_insult)
                .setMessage(R.string.delete_insult_confirmation)
                .setPositiveButton(android.R.string.ok) { _, _ ->
                    insultPropertiesObserverScope.cancel()
                    val job = insultInfoViewModel.deleteLocalInsult(insult)
                    disposableHandleContainer.add(job.invokeOnCompletion {
                        viewLifecycleOwner.lifecycleScope.launch {
                            Toast.makeText(requireContext(), R.string.insult_deleted, Toast.LENGTH_SHORT).show()
                            dismiss()
                        }
                    })
                }
                .setNegativeButton(android.R.string.cancel, null)
                .show()
        }
        insultPropertiesObserverScope.launch {
            insultInfoViewModel.getFavoriteStatus(insult).collect {
                if (it) {
                    with(binding.favorite) {
                        setCompoundDrawablesWithIntrinsicBounds(ContextCompat.getDrawable(requireContext(), R.drawable.favorite)?.tint(Color.RED), null, null, null)
                        text = getString(R.string.currently_favorited)
                        setOnClickListener(removeInsultFromFavorites)
                    }
                    binding.delete.visibility = View.GONE
                } else {
                    with(binding.favorite) {
                        setCompoundDrawablesWithIntrinsicBounds(R.drawable.favorite, 0, 0, 0)
                        text = getString(R.string.not_favorited)
                        setOnClickListener(addInsultToFavorites)
                    }
                    binding.delete.visibility = View.VISIBLE
                }
            }
        }
        binding.share.setOnClickListener {
            insult.share(requireContext())
        }
        binding.copyToClipboard.setOnClickListener {
            insult.copyToClipboard(requireContext())
        }
        if (!insult.createdBy.isNullOrBlank()) {
            binding.createdBy.text = insult.createdBy
        } else {
            binding.createdBy.visibility = View.GONE
        }
        if (!insult.comment.isNullOrBlank()) {
            binding.comment.text = insult.comment
        } else {
            binding.comment.visibility = View.GONE
        }
        binding.language.setText(insult.language.languageLabelResId)
        binding.createdTime.text = createInsultJsonFieldFromLocalDateTime(insult.created)

        insultPropertiesObserverScope.launch {
            insultInfoViewModel.getRating(insult).collect {
                if (it == null) {
                    with(binding.addDeleteRating) {
                        text = getString(R.string.add_rating)
                        setCompoundDrawablesWithIntrinsicBounds(R.drawable.add, 0, 0, 0)
                        setOnClickListener(showAddRatingDialog)
                    }
                    with(binding.ratingScore) {
                        visibility = View.GONE
                        setOnClickListener(null)
                    }
                    with(binding.ratingExplanation) {
                        visibility = View.GONE
                        setOnClickListener(null)
                    }
                } else {
                    binding.ratingScore.setOnClickListener { _ ->
                        findNavController().navigate(InsultInfoDialogDirections.actionNavigationInsultInfoToNavigationEditRatingScore(
                            ParcelableInsultRatingDto(it)
                        ))
                    }
                    binding.ratingExplanation.setOnClickListener { _ ->
                        findNavController().navigate(InsultInfoDialogDirections.actionNavigationInsultInfoToNavigationEditRatingExplanation(
                            ParcelableInsultRatingDto(it)
                        ))
                    }
                    with(binding.addDeleteRating) {
                        text = getString(R.string.delete_rating)
                        setCompoundDrawablesWithIntrinsicBounds(R.drawable.delete, 0, 0, 0)
                        setOnClickListener(deleteRating)
                    }
                    with(binding.ratingScore) {
                        visibility = View.VISIBLE
                        text = "%d".format(
                            AppCompatDelegate.getApplicationLocales()[0] ?: Locale.getDefault(),
                            it.rating.toInt()
                        )
                    }
                    binding.ratingExplanation.visibility = View.VISIBLE
                    if (it.explanation.isNullOrBlank()) {
                        with(binding.ratingExplanation) {
                            setCompoundDrawablesWithIntrinsicBounds(R.drawable.add, 0, 0, 0)
                            text = getString(R.string.add_explanation)
                        }
                    } else {
                        with(binding.ratingExplanation) {
                            setCompoundDrawablesWithIntrinsicBounds(R.drawable.chat, 0, 0, 0)
                            text = it.explanation
                        }
                    }
                }
            }
        }

        EditRatingScoreDialog.registerCallback(requireActivity(), viewLifecycleOwner) {
            val job = insultInfoViewModel.updateRating(insult, it)
            disposableHandleContainer.add(job.invokeOnCompletion {
                viewLifecycleOwner.lifecycleScope.launch {
                    Toast.makeText(requireContext(), R.string.successfully_updated_rating, Toast.LENGTH_SHORT).show()
                }
            })
        }

        EditRatingExplanationDialog.registerCallback(requireActivity(), viewLifecycleOwner) {
            val job = insultInfoViewModel.updateRating(insult, it)
            disposableHandleContainer.add(job.invokeOnCompletion {
                viewLifecycleOwner.lifecycleScope.launch {
                    Toast.makeText(requireContext(), R.string.successfully_updated_rating, Toast.LENGTH_SHORT).show()
                }
            })
        }

        AddRatingDialog.registerCallback(requireActivity(), viewLifecycleOwner) {
            val job = insultInfoViewModel.updateRating(insult, it)
            disposableHandleContainer.add(job.invokeOnCompletion {
                viewLifecycleOwner.lifecycleScope.launch {
                    Toast.makeText(requireContext(), R.string.successfully_added_rating, Toast.LENGTH_SHORT).show()
                }
            })
        }

    }
}