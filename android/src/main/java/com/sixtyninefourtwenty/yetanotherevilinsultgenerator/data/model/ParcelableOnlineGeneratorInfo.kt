package com.sixtyninefourtwenty.yetanotherevilinsultgenerator.data.model

import android.os.Parcel
import android.os.Parcelable
import androidx.core.os.ParcelCompat
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.dto.OnlineGeneratorInfo
import io.realm.kotlin.ext.toRealmDictionary

@JvmInline
value class ParcelableOnlineGeneratorInfo(val obj: OnlineGeneratorInfo) : Parcelable {

    constructor(parcel: Parcel) : this(
        OnlineGeneratorInfo(
            id = parcel.readString()!!,
            baseUrl = parcel.readString()!!,
            displayName = parcel.readString()!!,
            languageParameterName = parcel.readString(),
            extraQueryParams = mutableMapOf<String, String>().also {
                ParcelCompat.readMap(parcel, it, null, String::class.java, String::class.java)
            }.toRealmDictionary()
        )
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        with(parcel) {
            writeString(obj.id)
            writeString(obj.baseUrl)
            writeString(obj.displayName)
            writeString(obj.languageParameterName)
            writeMap(obj.extraQueryParams)
        }
    }

    override fun describeContents(): Int = 0

    companion object CREATOR : Parcelable.Creator<ParcelableOnlineGeneratorInfo> {
        override fun createFromParcel(parcel: Parcel): ParcelableOnlineGeneratorInfo {
            return ParcelableOnlineGeneratorInfo(parcel)
        }

        override fun newArray(size: Int): Array<ParcelableOnlineGeneratorInfo?> {
            return arrayOfNulls(size)
        }
    }


}