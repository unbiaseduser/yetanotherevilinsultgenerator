package com.sixtyninefourtwenty.yetanotherevilinsultgenerator.data.sortandfilter

import android.os.Parcel
import android.os.Parcelable
import androidx.core.os.ParcelCompat
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.sortandfilter.InsultSortOptions

@JvmInline
value class ParcelableInsultSortOptions(val obj: InsultSortOptions) : Parcelable {

    private constructor(parcel: Parcel) : this(
        InsultSortOptions(
            favoritesFirst = ParcelCompat.readBoolean(parcel),
            mode = parcel.readString()?.let { InsultSortOptions.Mode.valueOf(it) }
        )
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        with(parcel) {
            ParcelCompat.writeBoolean(this, obj.favoritesFirst)
            writeString(obj.mode?.name)
        }
    }

    override fun describeContents(): Int = 0

    companion object CREATOR : Parcelable.Creator<ParcelableInsultSortOptions> {
        override fun createFromParcel(parcel: Parcel): ParcelableInsultSortOptions {
            return ParcelableInsultSortOptions(parcel)
        }

        override fun newArray(size: Int): Array<ParcelableInsultSortOptions?> {
            return arrayOfNulls(size)
        }
    }


}