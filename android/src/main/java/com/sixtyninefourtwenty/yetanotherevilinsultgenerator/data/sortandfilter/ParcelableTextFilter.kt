package com.sixtyninefourtwenty.yetanotherevilinsultgenerator.data.sortandfilter

import android.os.Parcel
import android.os.Parcelable
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.sortandfilter.TextFilter

@JvmInline
value class ParcelableTextFilter(val obj: TextFilter) : Parcelable {

    private constructor(parcel: Parcel) : this(
        TextFilter(
            textToSearch = parcel.readString(),
            mode = TextFilter.Mode.valueOf(parcel.readString()!!)
        )
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        with(parcel) {
            writeString(obj.textToSearch)
            writeString(obj.mode.name)
        }
    }

    override fun describeContents(): Int = 0

    companion object CREATOR : Parcelable.Creator<ParcelableTextFilter> {
        override fun createFromParcel(parcel: Parcel): ParcelableTextFilter {
            return ParcelableTextFilter(parcel)
        }

        override fun newArray(size: Int): Array<ParcelableTextFilter?> {
            return arrayOfNulls(size)
        }
    }

}