package com.sixtyninefourtwenty.yetanotherevilinsultgenerator.data.sortandfilter

import android.os.Parcel
import android.os.Parcelable
import androidx.core.os.ParcelCompat
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.Language
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.sortandfilter.InsultFilterOptions

@JvmInline
value class ParcelableInsultFilterOptions(val obj: InsultFilterOptions) : Parcelable {

    private constructor(parcel: Parcel) : this(
        InsultFilterOptions(
            language = parcel.readString()?.let { Language.valueOf(it) },
            textFilter = ParcelCompat.readParcelable(parcel, null, ParcelableTextFilter::class.java)?.obj
        )
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        with(parcel) {
            writeString(obj.language?.name)
            writeParcelable(obj.textFilter?.let { ParcelableTextFilter(it) }, flags)
        }
    }

    override fun describeContents(): Int = 0

    companion object CREATOR : Parcelable.Creator<ParcelableInsultFilterOptions> {
        override fun createFromParcel(parcel: Parcel): ParcelableInsultFilterOptions {
            return ParcelableInsultFilterOptions(parcel)
        }

        override fun newArray(size: Int): Array<ParcelableInsultFilterOptions?> {
            return arrayOfNulls(size)
        }
    }


}