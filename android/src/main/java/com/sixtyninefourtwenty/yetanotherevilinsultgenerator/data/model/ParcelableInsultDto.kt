package com.sixtyninefourtwenty.yetanotherevilinsultgenerator.data.model

import android.os.Parcel
import android.os.Parcelable
import androidx.core.os.ParcelCompat
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.Language
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.dto.InsultDto
import kotlinx.datetime.toJavaLocalDateTime
import kotlinx.datetime.toKotlinLocalDateTime
import java.time.LocalDateTime

@JvmInline
value class ParcelableInsultDto(val obj: InsultDto) : Parcelable {

    private constructor(parcel: Parcel) : this(
        InsultDto(
            number = parcel.readInt(),
            language = Language.valueOf(parcel.readString()!!),
            insult = parcel.readString()!!,
            created = ParcelCompat.readSerializable(parcel, null, LocalDateTime::class.java)!!.toKotlinLocalDateTime(),
            shown = parcel.readInt(),
            createdBy = parcel.readString(),
            active = parcel.readInt(),
            comment = parcel.readString(),
            isFavorite = ParcelCompat.readBoolean(parcel),
            rating = ParcelCompat.readParcelable(parcel, null, ParcelableInsultRatingDto::class.java)?.obj
        )
    )

    override fun describeContents(): Int = 0

    override fun writeToParcel(dest: Parcel, flags: Int) {
        with(dest) {
            writeInt(obj.number)
            writeString(obj.language.name)
            writeString(obj.insult)
            writeSerializable(obj.created.toJavaLocalDateTime())
            writeInt(obj.shown)
            writeString(obj.createdBy)
            writeInt(obj.active)
            writeString(obj.comment)
            ParcelCompat.writeBoolean(this, obj.isFavorite)
            writeParcelable(obj.rating?.let { ParcelableInsultRatingDto(it) }, flags)
        }
    }

    companion object CREATOR : Parcelable.Creator<ParcelableInsultDto> {
        override fun createFromParcel(parcel: Parcel): ParcelableInsultDto {
            return ParcelableInsultDto(parcel)
        }

        override fun newArray(size: Int): Array<ParcelableInsultDto?> {
            return arrayOfNulls(size)
        }
    }
}