package com.sixtyninefourtwenty.yetanotherevilinsultgenerator.data.model

import android.os.Parcel
import android.os.Parcelable
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.dto.InsultRatingDto

@JvmInline
value class ParcelableInsultRatingDto(val obj: InsultRatingDto): Parcelable {

    constructor(parcel: Parcel) : this(
        InsultRatingDto(
            rating = parcel.readFloat(),
            explanation = parcel.readString()
        )
    )

    override fun describeContents(): Int = 0

    override fun writeToParcel(dest: Parcel, flags: Int) {
        with(dest) {
            writeFloat(obj.rating)
            writeString(obj.explanation)
        }
    }

    companion object CREATOR : Parcelable.Creator<ParcelableInsultRatingDto> {
        override fun createFromParcel(parcel: Parcel): ParcelableInsultRatingDto {
            return ParcelableInsultRatingDto(parcel)
        }

        override fun newArray(size: Int): Array<ParcelableInsultRatingDto?> {
            return arrayOfNulls(size)
        }
    }
}