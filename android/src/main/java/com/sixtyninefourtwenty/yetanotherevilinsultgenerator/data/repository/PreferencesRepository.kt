package com.sixtyninefourtwenty.yetanotherevilinsultgenerator.data.repository

import android.content.Context
import android.os.Build
import androidx.core.content.edit
import androidx.lifecycle.map
import androidx.preference.PreferenceManager
import androidx.work.Constraints
import androidx.work.NetworkType
import com.sixtyninefourtwenty.custompreferences.getLocalTime
import com.sixtyninefourtwenty.custompreferences.putLocalTime
import com.sixtyninefourtwenty.theming.LightDarkMode
import com.sixtyninefourtwenty.theming.ThemeColor
import com.sixtyninefourtwenty.theming.preferences.ThemingPreferencesSupplier
import com.sixtyninefourtwenty.theming.preferences.toThemingPreferencesSupplier
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.R
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.insultoftheday.InsultOfTheDayManager
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.OFFICIAL_EIG_BACKUP_URL
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.OFFICIAL_EIG_URL
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.Language
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.dto.InsultDto
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.repository.AndroidPreferences
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.repository.Preferences
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.utils.isDeviceOnOrOverSdk
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.utils.stringLiveData
import java.time.Duration
import java.time.LocalTime

class PreferencesRepository(
    context: Context,
    private val iotdManager: InsultOfTheDayManager
) : AndroidPreferences(context), ThemingPreferencesSupplier {

    private val delegate = preferences.toThemingPreferencesSupplier(context)

    override var md3: Boolean by delegate::md3
    override var lightDarkMode: LightDarkMode by delegate::lightDarkMode
    override var useM3CustomColorThemeOnAndroid12: Boolean by delegate::useM3CustomColorThemeOnAndroid12
    override var themeColor: ThemeColor by delegate::themeColor

    var savedIotdInsult: InsultDto?
        get() = getStringOrNull("insult")?.let { InsultDto.fromJson(it) }
        set(value) { putStringOrNull("insult", value?.toJson()) }

    var generatorLanguage: Language
        get() = Language.entries.first { it.languageCode == getString("generator_language", Language.ENGLISH.languageCode) }
        set(value) { putString("generator_language", value.languageCode) }

    val generatorLanguageLiveData = preferences.stringLiveData("generator_language", Language.ENGLISH.languageCode)
        .map { code -> Language.entries.first { it.languageCode == code } }

    var generatorId: String
        get() = getString("generator_id", OFFICIAL_EIG_URL)
        set(value) { putString("generator_id", value) }

    var iotdEnabled: Boolean
        get() = getBoolean("iotd_master_switch", false)
        set(value) {
            if (value) {
                iotdManager.enable(iotdTime)
            } else {
                iotdManager.disable()
            }
            putBoolean("iotd_master_switch", value)
        }

    var iotdTime: LocalTime
        get() = preferences.getLocalTime("iotd_time", LocalTime.of(9, 0))
        set(value) {
            if (iotdEnabled) {
                iotdManager.enableWithoutAffectingBootReceiver(iotdTime)
            }
            preferences.edit { putLocalTime("iotd_time", value) }
        }

    enum class WorkManagerConstraint(override val value: String) : Preferences.StringValue {
        UNMETERED_NETWORK("unmetered_network"),
        DEVICE_CHARGING("device_charging"),
        DEVICE_IDLE("device_idle"),
        BATTERY_NOT_LOW("battery_not_low"),
        STORAGE_NOT_LOW("storage_not_low")
    }

    var iotdWorkManagerConstraints: Iterable<WorkManagerConstraint>
        get() = WorkManagerConstraint.entries.getStringValueEnums("iotd_constraints")
        set(value) { putStringValueEnums("iotd_constraints", value) }

    enum class UpdateInterval(override val value: String, val duration: Duration) : Preferences.StringValue {
        ONE_HOUR("1h", Duration.ofHours(1)),
        TWO_HOURS("2h", Duration.ofHours(2)),
        THREE_HOURS("3h", Duration.ofHours(3)),
        FOUR_HOURS("4h", Duration.ofHours(4)),
        SIX_HOURS("6h", Duration.ofHours(6)),
        EIGHT_HOURS("8h", Duration.ofHours(8))
    }

    var numberOfInsultsUpdatedPeriodically: Int
        get() = getString("number_of_insults_updated_periodically", "0").toInt()
        set(value) { putString("number_of_insults_updated_periodically", value.toString()) }

    fun isPeriodicInsultUpdatesEnabled() = numberOfInsultsUpdatedPeriodically > 0

    var periodicInsultUpdateInterval: UpdateInterval
        get() = UpdateInterval.entries.getStringValueEnum("update_insults_periodically_interval", UpdateInterval.ONE_HOUR)
        set(value) { putStringValueEnum("update_insults_periodically_interval", value) }

    var periodicInsultUpdateConstraints: Iterable<WorkManagerConstraint>
        get() = WorkManagerConstraint.entries.getStringValueEnums("update_insults_periodically_constraints")
        set(value) { putStringValueEnums("update_insults_periodically_constraints", value) }

    var isThemingPreferencesMigrated: Boolean
        get() = getBoolean("theming_preferences_migrated", false)
        set(value) { putBoolean("theming_preferences_migrated", value) }

    var enableCustomGenerators: Boolean
        get() = getBoolean("enable_custom_generators", false)
        set(value) {
            if (!value) {
                coerceGeneratorIdToOfficial()
            }
            putBoolean("enable_custom_generators", value)
        }

    fun coerceGeneratorIdToOfficial() {
        val currentGeneratorId = generatorId
        if (currentGeneratorId != OFFICIAL_EIG_URL && currentGeneratorId != OFFICIAL_EIG_BACKUP_URL) {
            generatorId = OFFICIAL_EIG_URL
        }
    }

    fun performMigrations() {
        val oldGeneratorUrlValue = getString("generator_url", "")
        if (oldGeneratorUrlValue.isNotEmpty()) {
            val newGeneratorIdValue = when (oldGeneratorUrlValue) {
                "https://evilinsult.com/", "main" -> OFFICIAL_EIG_URL
                "https://slave.evilinsult.com/", "backup" -> OFFICIAL_EIG_BACKUP_URL
                else -> OFFICIAL_EIG_URL
            }
            putString("generator_id", newGeneratorIdValue)
            remove("generator_url")
        }
    }

    companion object {
        fun initializeInitialValues(context: Context) {
            PreferenceManager.setDefaultValues(context, R.xml.preferences_root, false)
        }
    }

}

fun Iterable<PreferencesRepository.WorkManagerConstraint>.createWorkManagerConstraints(isNetworkRequired: Boolean = true) = Constraints.Builder().apply {
    if (isNetworkRequired) {
        if (contains(PreferencesRepository.WorkManagerConstraint.UNMETERED_NETWORK)) {
            setRequiredNetworkType(NetworkType.UNMETERED)
        } else {
            setRequiredNetworkType(NetworkType.CONNECTED)
        }
    }
    if (contains(PreferencesRepository.WorkManagerConstraint.DEVICE_CHARGING)) {
        setRequiresCharging(true)
    }
    if (contains(PreferencesRepository.WorkManagerConstraint.DEVICE_IDLE) && isDeviceOnOrOverSdk(Build.VERSION_CODES.M)) {
        setRequiresDeviceIdle(true)
    }
    if (contains(PreferencesRepository.WorkManagerConstraint.BATTERY_NOT_LOW)) {
        setRequiresBatteryNotLow(true)
    }
    if (contains(PreferencesRepository.WorkManagerConstraint.STORAGE_NOT_LOW)) {
        setRequiresStorageNotLow(true)
    }
}.build()
