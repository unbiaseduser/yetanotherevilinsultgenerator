package com.sixtyninefourtwenty.yetanotherevilinsultgenerator.utils

import android.content.Context
import com.skydoves.balloon.ArrowPositionRules
import com.skydoves.balloon.Balloon
import com.skydoves.balloon.createBalloon

fun createBalloonWithStandardSettings(
    context: Context,
    block: Balloon.Builder.() -> Unit
) = createBalloon(context) {
    setTextSize(14F)
    setPadding(10)
    setArrowPositionRules(ArrowPositionRules.ALIGN_ANCHOR)
    block(this)
}