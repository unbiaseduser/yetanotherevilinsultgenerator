package com.sixtyninefourtwenty.yetanotherevilinsultgenerator.utils

import android.content.SharedPreferences
import androidx.lifecycle.LiveData

sealed class SharedPreferencesLiveData<T : Any>(
    protected val sharedPrefs: SharedPreferences,
    private val key: String,
    private val defValue: T
) : LiveData<T>() {

    private val preferenceChangeListener = SharedPreferences.OnSharedPreferenceChangeListener { sharedPreferences, key ->
        if (key == this.key) {
            value = getValueFromPreferences(key, defValue)
        }
    }

    abstract fun getValueFromPreferences(key: String, defValue: T): T

    override fun onActive() {
        super.onActive()
        value = getValueFromPreferences(key, defValue)
        sharedPrefs.registerOnSharedPreferenceChangeListener(preferenceChangeListener)
    }

    override fun onInactive() {
        sharedPrefs.unregisterOnSharedPreferenceChangeListener(preferenceChangeListener)
        super.onInactive()
    }

}

class SharedPreferencesStringLiveData(
    sharedPrefs: SharedPreferences,
    key: String,
    defValue: String
) : SharedPreferencesLiveData<String>(sharedPrefs, key, defValue) {

    override fun getValueFromPreferences(key: String, defValue: String): String {
        return sharedPrefs.getString(key, defValue)!!
    }

}

fun SharedPreferences.stringLiveData(key: String, defValue: String) = SharedPreferencesStringLiveData(this, key, defValue)