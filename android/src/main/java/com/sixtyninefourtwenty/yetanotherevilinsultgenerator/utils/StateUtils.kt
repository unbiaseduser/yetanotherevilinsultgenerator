package com.sixtyninefourtwenty.yetanotherevilinsultgenerator.utils

import android.os.Bundle
import androidx.core.os.BundleCompat
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.data.model.ParcelableInsultDto
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.ViewState
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.dto.InsultDto

fun Bundle.toViewState(): ViewState<InsultDto> {
    return when (getString("type")) {
        "content" -> ViewState.Content(
            BundleCompat.getParcelable(this, "data", ParcelableInsultDto::class.java)!!.obj
        )
        "error" -> ViewState.Error(
            BundleCompat.getSerializable(this, "error", Exception::class.java)!!
        )
        "loading" -> ViewState.Loading
        "empty" -> ViewState.Empty
        else -> ViewState.Empty
    }
}

fun ViewState<InsultDto>.toBundle(): Bundle {
    val b = Bundle()
    when (this) {
        is ViewState.Content -> {
            b.putString("type", "content")
            b.putParcelable("data", ParcelableInsultDto(data))
        }
        is ViewState.Error -> {
            b.putString("type", "error")
            b.putSerializable("error", exception)
        }
        is ViewState.Loading -> {
            b.putString("type", "loading")
        }
        is ViewState.Empty -> {
            b.putString("type", "empty")
        }
    }
    return b
}