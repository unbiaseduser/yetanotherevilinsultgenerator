package com.sixtyninefourtwenty.yetanotherevilinsultgenerator.utils

import android.content.Context
import android.graphics.drawable.Drawable
import android.widget.ImageView
import androidx.appcompat.widget.AppCompatEditText
import com.sixtyninefourtwenty.bottomsheetalertdialog.view.BottomSheetAlertDialogContentView
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.R

fun AppCompatEditText.isInputEmpty() = text == null || text.toString().isBlank()

fun AppCompatEditText.getInput() = if (isInputEmpty()) "" else text.toString().trim()

var ImageView.drawableContent: Drawable?
    get() = drawable
    set(value) = setImageDrawable(value)

fun BottomSheetAlertDialogContentView.bottomPaddedWithDefaultDimension(context: Context) =
    bottomPadded(context.resources.getDimensionPixelSize(R.dimen.bsad_content_view_bottom_padding))
