package com.sixtyninefourtwenty.yetanotherevilinsultgenerator.utils

import android.app.ActivityOptions
import android.os.Build

fun pendingIntentGetActivityOptions() = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.UPSIDE_DOWN_CAKE) {
    ActivityOptions.makeBasic()
        .setPendingIntentCreatorBackgroundActivityStartMode(ActivityOptions.MODE_BACKGROUND_ACTIVITY_START_ALLOWED)
        .toBundle()
} else {
    null
}