package com.sixtyninefourtwenty.yetanotherevilinsultgenerator.utils

import android.content.ClipData
import android.content.Context
import android.content.Intent
import androidx.annotation.StringRes
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.R
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.Language
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.Language.AFRIKAANS
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.Language.ARABIC
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.Language.BENGALI
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.Language.BULGARIAN
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.Language.CATALAN
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.Language.CHINESE
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.Language.CZECH
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.Language.DANISH
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.Language.DUTCH
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.Language.ENGLISH
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.Language.FINNISH
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.Language.FRENCH
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.Language.GERMAN
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.Language.GREEK
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.Language.HEBREW
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.Language.HINDI
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.Language.HUNGARIAN
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.Language.INDONESIAN
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.Language.ITALIAN
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.Language.JAPANESE
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.Language.JAVANESE
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.Language.KANNADA
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.Language.KOREAN
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.Language.LATIN
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.Language.MARATHI
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.Language.NORWEGIAN
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.Language.PERSIAN
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.Language.POLISH
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.Language.PORTUGUESE
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.Language.PUNJABI
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.Language.ROMANIAN
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.Language.RUSSIAN
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.Language.SERBIAN
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.Language.SINHALA
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.Language.SPANISH
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.Language.SWAHILI
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.Language.SWEDISH
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.Language.TAMIL
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.Language.TELUGU
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.Language.THAI
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.Language.TURKISH
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.Language.UKRAINIAN
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.Language.URDU
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.Language.VIETNAMESE
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.dto.InsultDto

fun InsultDto.share(context: Context) {
    context.startActivity(Intent.createChooser(Intent(Intent.ACTION_SEND)
        .putExtra(Intent.EXTRA_TEXT, insult)
        .setType("text/plain"), null))
}

fun InsultDto.copyToClipboard(context: Context) {
    context.clipboardManager.setPrimaryClip(ClipData.newPlainText("insult", insult))
}

@get:StringRes
val Language.languageLabelResId: Int get() {
    return when (this) {
        ENGLISH -> R.string.english
        SPANISH -> R.string.spanish
        CHINESE -> R.string.chinese
        HINDI -> R.string.hindi
        ARABIC -> R.string.arabic
        PORTUGUESE -> R.string.portuguese
        BENGALI -> R.string.bengali
        RUSSIAN -> R.string.russian
        JAPANESE -> R.string.japanese
        JAVANESE -> R.string.javanese
        SWAHILI -> R.string.swahili
        GERMAN -> R.string.german
        KOREAN -> R.string.korean
        FRENCH -> R.string.french
        TELUGU -> R.string.telugu
        MARATHI -> R.string.marathi
        TURKISH -> R.string.turkish
        TAMIL -> R.string.tamil
        VIETNAMESE -> R.string.vietnamese
        URDU -> R.string.urdu
        GREEK -> R.string.greek
        ITALIAN -> R.string.italian
        CZECH -> R.string.czech
        FINNISH -> R.string.finnish
        AFRIKAANS -> R.string.afrikaans
        BULGARIAN -> R.string.bulgarian
        CATALAN -> R.string.catalan
        DANISH -> R.string.danish
        DUTCH -> R.string.dutch
        HEBREW -> R.string.hebrew
        HUNGARIAN -> R.string.hungarian
        INDONESIAN -> R.string.indonesian
        KANNADA -> R.string.kannada
        NORWEGIAN -> R.string.norwegian
        PERSIAN -> R.string.persian
        POLISH -> R.string.polish
        PUNJABI -> R.string.punjabi
        ROMANIAN -> R.string.romanian
        SERBIAN -> R.string.serbian
        SINHALA -> R.string.sinhala
        SWEDISH -> R.string.swedish
        THAI -> R.string.thai
        UKRAINIAN -> R.string.ukrainian
        LATIN -> R.string.latin
    }
}
