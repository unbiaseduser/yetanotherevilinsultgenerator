package com.sixtyninefourtwenty.yetanotherevilinsultgenerator.insultoftheday

import android.content.ComponentName
import android.content.Context
import android.content.pm.PackageManager
import java.time.LocalTime

class InsultOfTheDayManager(
    private val context: Context
) {

    private fun setBootReceiverEnabled(enabled: Boolean) {
        val cn = ComponentName(context, InsultOfTheDayBootReceiver::class.java)
        val pm = context.packageManager
        val enabledState = if (enabled) {
            PackageManager.COMPONENT_ENABLED_STATE_ENABLED
        } else {
            PackageManager.COMPONENT_ENABLED_STATE_DISABLED
        }
        pm.setComponentEnabledSetting(
            cn,
            enabledState,
            PackageManager.DONT_KILL_APP
        )
    }

    fun enableWithoutAffectingBootReceiver(time: LocalTime) {
        InsultOfTheDayBroadcastReceiver.registerDailyInvocation(context, time)
    }

    fun enable(time: LocalTime) {
        enableWithoutAffectingBootReceiver(time)
        setBootReceiverEnabled(true)
    }

    fun disable() {
        InsultOfTheDayBroadcastReceiver.cancelDailyInvocation(context)
        setBootReceiverEnabled(false)
    }

}