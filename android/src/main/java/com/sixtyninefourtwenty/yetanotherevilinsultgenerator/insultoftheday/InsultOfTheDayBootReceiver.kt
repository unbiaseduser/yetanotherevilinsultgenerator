package com.sixtyninefourtwenty.yetanotherevilinsultgenerator.insultoftheday

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.utils.myApplication

class InsultOfTheDayBootReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        if (intent.action == Intent.ACTION_BOOT_COMPLETED) {
            val app = context.myApplication
            val prefs = app.preferencesRepository
            if (prefs.iotdEnabled) {
                app.iotdManager.enableWithoutAffectingBootReceiver(prefs.iotdTime)
            }
        }
    }

}