package com.sixtyninefourtwenty.yetanotherevilinsultgenerator

import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.graphics.Color
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NotificationChannelCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.content.IntentCompat
import androidx.core.view.WindowInsetsControllerCompat
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.sixtyninefourtwenty.theming.applyTheming
import com.sixtyninefourtwenty.theming.preferences.copyFromDefaultThemingPreferences
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.data.model.ParcelableInsultDto
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.data.repository.PreferencesRepository
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.databinding.ActivityMainBinding
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.dto.InsultDto
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.ui.generator.GeneratorFragmentDirections
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.utils.ACTION_SHOW_INFO_FROM_IOTD_NOTIFICATION
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.utils.IOTD_EXTRA_KEY
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.utils.IOTD_NOTIFICATION_CHANNEL_ID
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.utils.myApplication
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        createIotdNotificationChannel()
        setInitialPreferences()
        val prefs = myApplication.preferencesRepository
        prefs.performMigrations()
        if (!prefs.isThemingPreferencesMigrated) {
            copyFromDefaultThemingPreferences(
                this,
                copyMd3 = { prefs.md3 = it },
                copyThemeColor = { prefs.themeColor = it },
                copyLightDarkMode = { prefs.lightDarkMode = it },
                copyUseM3CustomColorThemeOnAndroid12 = { prefs.useM3CustomColorThemeOnAndroid12 = it }
            )
            prefs.isThemingPreferencesMigrated = true
        }
        applyTheming(
            material2ThemeStyleRes = R.style.Theme_YetAnotherEvilInsultGenerator,
            material3CustomColorsThemeStyleRes = R.style.Theme_YetAnotherEvilInsultGenerator_Material3_Android11,
            material3DynamicColorsThemeStyleRes = R.style.Theme_YetAnotherEvilInsultGenerator_Material3,
            preferencesSupplier = prefs
        )
        val w = window!!
        @Suppress("DEPRECATION")
        w.statusBarColor = Color.TRANSPARENT
        val isDarkMode = resources.configuration.uiMode and Configuration.UI_MODE_NIGHT_MASK == Configuration.UI_MODE_NIGHT_YES
        val wic = WindowInsetsControllerCompat(w, w.decorView)
        wic.isAppearanceLightStatusBars = !isDarkMode

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.toolbar)

        val navView: BottomNavigationView = binding.navView

        val navController = findNavController(R.id.nav_host_fragment_activity_main)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        val appBarConfiguration = AppBarConfiguration(
            setOf(R.id.navigation_generator, R.id.navigation_offline, R.id.navigation_settings)
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)

        handleLaunchedIntent()
    }

    private fun setInitialPreferences() {
        PreferencesRepository.initializeInitialValues(this)
    }

    private fun handleLaunchedIntent() {
        if (intent.action == ACTION_SHOW_INFO_FROM_IOTD_NOTIFICATION) {
            val navController = findNavController(R.id.nav_host_fragment_activity_main)
            if (navController.currentDestination?.id == R.id.navigation_generator) {
                val parcelableInsultDto = IntentCompat.getParcelableExtra(intent, IOTD_EXTRA_KEY, ParcelableInsultDto::class.java)!!
                val insult = parcelableInsultDto.obj
                lifecycleScope.launch {
                    myApplication.insultsRepository.upsertInsult(insult)
                    navController.navigate(GeneratorFragmentDirections.actionNavigationGeneratorToNavigationInsultInfo(
                        parcelableInsultDto
                    ))
                }
            }
        }
    }

    private fun createIotdNotificationChannel() {
        NotificationManagerCompat.from(this)
            .createNotificationChannel(
                NotificationChannelCompat.Builder(IOTD_NOTIFICATION_CHANNEL_ID, NotificationManagerCompat.IMPORTANCE_DEFAULT)
                .setName(getString(R.string.insult_of_the_day))
                .setDescription(getString(R.string.iotd_notification_channel_description))
                .build())
    }

    companion object {
        fun createActionShowIotdIntent(context: Context, insult: InsultDto) = Intent(ACTION_SHOW_INFO_FROM_IOTD_NOTIFICATION)
            .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            .setClass(context, MainActivity::class.java)
            .putExtra(IOTD_EXTRA_KEY, ParcelableInsultDto(insult))
    }
}