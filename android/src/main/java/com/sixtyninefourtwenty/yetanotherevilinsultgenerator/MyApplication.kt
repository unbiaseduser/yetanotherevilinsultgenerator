package com.sixtyninefourtwenty.yetanotherevilinsultgenerator

import android.app.Application
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.data.repository.PreferencesRepository
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.insultoftheday.InsultOfTheDayManager
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.OfficialOnlineInsultGeneratorService
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.database.CustomGeneratorInfoDatabase
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.database.OfflineInsultsDatabase
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.datasource.InsultsLocalDataSourceImpl
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.datasource.InsultsRemoteDataSourceImpl
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.Language
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.repository.CustomGeneratorInfoRepository
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.repository.CustomGeneratorInfoRepositoryImpl
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.repository.InsultsRepository
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.repository.InsultsRepositoryImpl
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob

class MyApplication : Application() {
    private val scope = CoroutineScope(SupervisorJob())
    private val database by lazy { OfflineInsultsDatabase.newInstance() }
    val iotdManager by lazy(LazyThreadSafetyMode.NONE) { InsultOfTheDayManager(this) }
    val preferencesRepository by lazy { PreferencesRepository(this, iotdManager) }
    val customGeneratorInfoRepository: CustomGeneratorInfoRepository by lazy {
        CustomGeneratorInfoRepositoryImpl(
            CustomGeneratorInfoDatabase.newInstance(),
            scope
        )
    }
    val insultsRepository: InsultsRepository by lazy {
        InsultsRepositoryImpl(
            listOf(
                InsultsRemoteDataSourceImpl(
                    { getString(R.string.main) },
                    OfficialOnlineInsultGeneratorService(OfficialOnlineInsultGeneratorService.Url.MAIN)
                ),
                InsultsRemoteDataSourceImpl(
                    { getString(R.string.backup) },
                    OfficialOnlineInsultGeneratorService(OfficialOnlineInsultGeneratorService.Url.BACKUP)
                )
            ),
            customGeneratorInfoRepository,
            InsultsLocalDataSourceImpl(database),
            scope
        )
    }

    suspend fun generateRemoteInsult(id: String, language: Language) = runCatching {
        insultsRepository.generateRemoteInsult(id, language)
    }

}