package com.sixtyninefourtwenty.yetanotherevilinsultgenerator

import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.database.OfflineInsultsDatabase
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.Insult
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.InsultRating
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import java.util.stream.Stream

class OfflineInsultsDatabaseTest {

    private lateinit var db: OfflineInsultsDatabase

    @BeforeEach
    fun setUp() {
        db = OfflineInsultsDatabase.newInMemoryInstance()
    }

    @AfterEach
    fun tearDown() {
        db.close()
    }

    @ParameterizedTest
    @MethodSource("updateInsultArgs")
    fun whenUpdatingAnInsultThenCustomPropertiesFromTheOriginalOneShouldBeKept(firstInsult: Insult, secondInsult: Insult) = runTest {
        // Preconditions
        assertEquals(firstInsult.number, secondInsult.number)
        assertNotEquals(firstInsult.insult, secondInsult.insult)
        assertTrue(firstInsult.isFavorite != secondInsult.isFavorite || firstInsult.rating != secondInsult.rating)

        db.upsertInsult(firstInsult)
        db.upsertInsult(secondInsult)
        assertEquals(secondInsult.insult, db.getInsultByNumber(firstInsult.number).insult)
        assertEquals(firstInsult.rating, db.getInsultByNumber(firstInsult.number).rating)
        assertEquals(firstInsult.isFavorite, db.getInsultByNumber(firstInsult.number).isFavorite)
    }

    companion object {
        @JvmStatic
        fun updateInsultArgs(): Stream<Arguments> {
            return Stream.of(
                Arguments.of(
                    Insult().apply {
                        number = 1
                        insult = "foo"
                        isFavorite = true
                    },
                    Insult().apply {
                        number = 1
                        insult = "bar"
                    }
                ),
                Arguments.of(
                    Insult().apply {
                        number = 2
                        insult = "foo"
                        rating = InsultRating(rating = 1F, explanation = "why not")
                    },
                    Insult().apply {
                        number = 2
                        insult = "bar"
                    }
                ),
                Arguments.of(
                    Insult().apply {
                        number = 3
                        insult = "foo"
                        isFavorite = true
                        rating = InsultRating(rating = 1F, explanation = "why not")
                    },
                    Insult().apply {
                        number = 3
                        insult = "bar"
                    }
                )
            )
        }
    }

}