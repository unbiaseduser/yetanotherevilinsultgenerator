import com.android.build.OutputFile
import com.android.build.gradle.api.ApkVariantOutput

plugins {
    alias(libs.plugins.androidApplication)
    alias(libs.plugins.kotlinAndroid)
    id("androidx.navigation.safeargs.kotlin")
    id("kotlin-parcelize")
    alias(libs.plugins.androidJunit5)
    id("tech.apter.junit5.jupiter.robolectric-extension-gradle-plugin") version "0.7.0"
}

android {
    namespace = "com.sixtyninefourtwenty.yetanotherevilinsultgenerator"
    compileSdk = 35

    defaultConfig {
        applicationId = "com.sixtyninefourtwenty.yetanotherevilinsultgenerator"
        minSdk = 21
        targetSdk = 35
        versionCode = 1
        versionName = "1.0"

        setProperty("archivesBaseName", "Yet Another Evil Insult Generator v$versionName")

        resourceConfigurations += listOf("en", "vi")

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        testInstrumentationRunnerArguments["runnerBuilder"] = "de.mannodermaus.junit5.AndroidJUnit5Builder"
    }

    signingConfigs {
        create("release") {
            storeFile = findProperty("androidStoreFile")?.let { File(it.toString()) }
            keyAlias = findProperty("androidKeyAlias")?.toString()
            storePassword = findProperty("androidStorePassword")?.toString()
            keyPassword = findProperty("androidKeyPassword")?.toString()
        }
    }

    splits {
        abi {
            isEnable = true
            reset()
            include("armeabi-v7a", "arm64-v8a", "x86", "x86_64")
            isUniversalApk = true
        }
    }

    buildTypes {
        release {
            signingConfig = findProperty("androidStoreFile")?.let { signingConfigs["release"] }
            isMinifyEnabled = true
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
        debug {
            applicationIdSuffix = ".debug"
            versionNameSuffix = "-debug"
        }
    }
    compileOptions {
        isCoreLibraryDesugaringEnabled = true
        sourceCompatibility = JavaVersion.VERSION_17
        targetCompatibility = JavaVersion.VERSION_17
    }
    kotlinOptions {
        jvmTarget = "17"
    }
    buildFeatures {
        buildConfig = true
        viewBinding = true
    }

    val abiCodes = mapOf("armeabi-v7a" to 0, "arm64-v8a" to 1, "x86" to 2, "x86_64" to 3)

    @Suppress("DEPRECATION")
    applicationVariants.configureEach {
        outputs.configureEach {
            val abi = filters.find { it.filterType == OutputFile.ABI }?.identifier
            if (abi != null) {
                if (this is ApkVariantOutput) {
                    versionCodeOverride = defaultConfig.versionCode!! * 10 + abiCodes[abi]!!
                }
            }
        }
    }
}

dependencies {

    implementation(project(":shared"))
    implementation(libs.kotlinx.coroutines.android)
    implementation(libs.core.ktx)
    implementation(libs.appcompat)
    implementation(libs.material)
    implementation(libs.constraintlayout)
    implementation(libs.lifecycle.livedata.ktx)
    implementation(libs.lifecycle.viewmodel.ktx)
    implementation(libs.navigation.fragment.ktx)
    implementation(libs.navigation.ui.ktx)
    implementation(libs.preference.ktx)
    implementation(libs.recyclerview)
    implementation(libs.recyclerview.selection)
    implementation(libs.work.runtime.ktx)
    implementation(libs.multistateview)
    implementation(libs.balloon)
    implementation(libs.flowlayout)
    implementation(libs.bottomsheetalertdialog)
    implementation(libs.basefragments)
    implementation(libs.custompreferencesthemingintegration)
    coreLibraryDesugaring(libs.desugar.jdk.libs)
    testImplementation(libs.androidx.test.ext.junit)
    testImplementation(libs.junit)
    testImplementation(libs.junit.jupiter.api)
    testRuntimeOnly(libs.junit.jupiter.engine)
    testRuntimeOnly(libs.junit.vintage.engine)
    testImplementation(libs.junit.jupiter.params)
    testImplementation(libs.robolectric)
    testImplementation(libs.espresso.core)
    testImplementation(libs.espresso.intents)
    testImplementation(libs.kotlinx.coroutines.test)
    testImplementation(libs.mockwebserver)
    androidTestImplementation(libs.junit)
    androidTestImplementation(libs.androidx.test.ext.junit)
    androidTestImplementation(libs.espresso.core)
    androidTestImplementation(libs.junit.jupiter.api)
    androidTestRuntimeOnly(libs.junit.jupiter.engine)
    androidTestImplementation(libs.junit.jupiter.params)
    androidTestImplementation(libs.android.junit5.test.core)
    androidTestImplementation(libs.kotlinx.coroutines.test)
    androidTestRuntimeOnly(libs.android.junit5.test.runner)
}