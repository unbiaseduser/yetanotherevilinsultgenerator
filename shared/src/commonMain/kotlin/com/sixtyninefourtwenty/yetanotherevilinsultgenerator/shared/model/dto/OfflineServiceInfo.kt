package com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.dto

data class OfflineServiceInfo(
    override val id: String,
    val insults: List<InsultDto>
) : ServiceInfo
