package com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.database

import io.realm.kotlin.Realm
import io.realm.kotlin.RealmConfiguration

sealed class RealmBackedDatabase(configuration: RealmConfiguration) {

    protected val realm: Realm = Realm.open(configuration)

    fun close() = realm.close()

}