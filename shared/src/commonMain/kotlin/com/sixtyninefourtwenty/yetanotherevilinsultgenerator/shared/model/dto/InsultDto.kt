package com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.dto

import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.createInsultJsonFieldFromLocalDateTime
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.createLocalDateTimeFromInsultJsonField
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.Insult
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.Language
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.serializer.InsultSerializer
import kotlinx.datetime.LocalDateTime
import kotlinx.serialization.json.Json

data class InsultDto(
    val number: Int,
    val language: Language,
    val insult: String,
    val created: LocalDateTime,
    val shown: Int,
    val createdBy: String?,
    val active: Int,
    val comment: String?,
    val isFavorite: Boolean,
    val rating: InsultRatingDto?
) {

    internal constructor(insult: Insult) : this(
        number = insult.number,
        language = insult.getLanguage(),
        insult = insult.insult,
        created = createLocalDateTimeFromInsultJsonField(insult.created),
        shown = insult.shown,
        createdBy = insult.createdBy,
        active = insult.active,
        comment = insult.comment,
        isFavorite = insult.isFavorite,
        rating = insult.rating?.let { InsultRatingDto(it) }
    )

    internal fun toEntity() = Insult(
        number = this.number,
        language = this.language.languageCode,
        insult = this.insult,
        created = createInsultJsonFieldFromLocalDateTime(this.created),
        shown = this.shown,
        createdBy = this.createdBy,
        active = this.active,
        comment = this.comment,
        isFavorite = this.isFavorite,
        rating = this.rating?.toEntity()
    )

    fun toJson() = Json.encodeToString(InsultSerializer, toEntity())

    companion object {
        fun fromJson(json: String) = InsultDto(Json.decodeFromString(InsultSerializer, json))
    }

}
