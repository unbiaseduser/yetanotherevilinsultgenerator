package com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.repository

import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.database.CustomGeneratorInfoDatabase
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.dto.OnlineGeneratorInfo
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.withContext

interface CustomGeneratorInfoRepository {
    val allOnlineInfo: Flow<List<OnlineGeneratorInfo>>
    suspend fun getAllOnlineInfoSnapshot(): List<OnlineGeneratorInfo>
    suspend fun upsertOnlineInfo(info: OnlineGeneratorInfo)
    suspend fun deleteOnlineInfo(id: String)
    suspend fun deleteOnlineInfo(info: OnlineGeneratorInfo) = deleteOnlineInfo(info.id)
}

class CustomGeneratorInfoRepositoryImpl(
    private val db: CustomGeneratorInfoDatabase,
    private val scope: CoroutineScope
) : CustomGeneratorInfoRepository {

    override val allOnlineInfo: Flow<List<OnlineGeneratorInfo>> = db.allOnlineInfo.map { info -> info.map { OnlineGeneratorInfo(it) } }

    override suspend fun getAllOnlineInfoSnapshot(): List<OnlineGeneratorInfo> {
        return withContext(Dispatchers.IO) {
            db.getAllOnlineInfoSnapshot().map { OnlineGeneratorInfo(it) }
        }
    }

    override suspend fun upsertOnlineInfo(info: OnlineGeneratorInfo) = withContext(scope.coroutineContext) {
        db.upsertOnlineInfo(info.toEntity())
    }

    override suspend fun deleteOnlineInfo(id: String) {
        db.deleteOnlineInfo(id)
    }

}