package com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.dto

import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.OfflineInsultGeneratorService
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.datasource.InsultsRemoteDataSource
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.datasource.InsultsRemoteDataSourceImpl
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.OfflineGeneratorInfoEntity
import io.realm.kotlin.ext.realmListOf

data class OfflineGeneratorInfo(
    override val id: String,
    override val displayName: String,
    val insults: List<InsultDto>
) : GeneratorInfo {

    internal constructor(info: OfflineGeneratorInfoEntity) : this(
        id = info.id,
        displayName = info.displayName,
        insults = info.insults.map { InsultDto(it) }
    )

    internal constructor(
        displayName: String,
        serviceInfo: OfflineServiceInfo
    ) : this(
        id = serviceInfo.id,
        displayName = displayName,
        insults = serviceInfo.insults
    )

    internal fun toEntity() = OfflineGeneratorInfoEntity(
        id = this.id,
        displayName = this.displayName,
        insults = this.insults.mapTo(realmListOf()) { it.toEntity() }
    )

    fun toOfflineGenerator(): InsultsRemoteDataSource = InsultsRemoteDataSourceImpl(
        constantDisplayName = displayName,
        service = OfflineInsultGeneratorService(
            id = this.id,
            insults = this.insults.map { it.toEntity() }
        )
    )

}