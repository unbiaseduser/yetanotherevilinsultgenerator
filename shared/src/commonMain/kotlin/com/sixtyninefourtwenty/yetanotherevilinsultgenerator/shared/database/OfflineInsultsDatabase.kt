package com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.database

import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.Insult
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.InsultRating
import io.realm.kotlin.RealmConfiguration
import io.realm.kotlin.TypedRealm
import io.realm.kotlin.ext.query
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class OfflineInsultsDatabase private constructor(
    configuration: RealmConfiguration
) : RealmBackedDatabase(configuration) {

    companion object {
        fun newInstance() = OfflineInsultsDatabase(RealmConfiguration.create(setOf(Insult::class, InsultRating::class)))
        fun newInMemoryInstance() = OfflineInsultsDatabase(RealmConfiguration.Builder(setOf(Insult::class, InsultRating::class))
            .inMemory()
            .build())
    }

    val allAvailableInsults: Flow<List<Insult>> = realm.query<Insult>().asFlow().map { it.list }

    suspend fun upsertInsult(insult: Insult) {
        realm.write {
            queryInsultByNumberInternal(insult.number).find()?.copyRegularPropertiesFrom(insult) ?: copyToRealm(insult)
        }
    }

    private fun throwInsultNotFoundByNumberException(number: Int): Nothing =
        throw IllegalArgumentException("Insult with number $number doesn't exist in db")

    private fun TypedRealm.queryInsultByNumberInternal(number: Int) = query<Insult>("number == $0", number).first()

    private fun queryInsultByNumber(number: Int) = realm.queryInsultByNumberInternal(number)

    private fun queryInsultByNumberAsFlow(number: Int) = queryInsultByNumber(number)
        .asFlow()
        .map { it.obj ?: throwInsultNotFoundByNumberException(number) }

    fun getFavoriteStatus(number: Int) = queryInsultByNumberAsFlow(number)
        .map { it.isFavorite }

    fun getRating(number: Int) = queryInsultByNumberAsFlow(number)
        .map { it.rating }

    fun getInsultByNumber(number: Int) = queryInsultByNumber(number).find() ?: throwInsultNotFoundByNumberException(number)

    private suspend fun updateInsultProperties(number: Int, block: (Insult) -> Unit) {
        val insult = queryInsultByNumber(number).find()
        if (insult != null) {
            realm.write {
                findLatest(insult)?.let(block)
            }
        }
    }

    suspend fun updateFavoriteStatus(number: Int, isFavorite: Boolean) = updateInsultProperties(number) {
        it.isFavorite = isFavorite
    }

    suspend fun updateRating(number: Int, rating: InsultRating?) = updateInsultProperties(number) {
        it.rating = rating
    }

    suspend fun deleteInsult(number: Int) {
        val insult = queryInsultByNumber(number).find()
        if (insult != null) {
            realm.write {
                findLatest(insult)?.let { delete(it) }
            }
        }
    }

    suspend fun deleteInsults(numbers: IntArray) {
        if (numbers.any()) {
            realm.write {
                numbers.forEach { number ->
                    val insult = queryInsultByNumber(number).find()
                    if (insult != null) {
                        findLatest(insult)?.let { delete(it) }
                    }
                }
            }
        }
    }

}