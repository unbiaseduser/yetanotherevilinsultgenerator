package com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared

import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.Insult
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.Language
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.dto.InsultDto
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.dto.OfflineServiceInfo
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.dto.OnlineServiceInfo
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.dto.ServiceInfo
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.serializer.InsultSerializer
import io.ktor.client.HttpClient
import io.ktor.client.request.get
import io.ktor.client.statement.bodyAsText
import kotlinx.serialization.json.Json

interface InsultGeneratorService {
    val id: String
    suspend fun generate(language: Language): Insult
    fun getInfo(): ServiceInfo
}

@Suppress("unused")
class OfflineInsultGeneratorService(
    override val id: String,
    private val insults: Iterable<Insult>
) : InsultGeneratorService {

    override suspend fun generate(language: Language): Insult {
        return insults.filter { it.getLanguage() == language }.random()
    }

    override fun getInfo(): ServiceInfo {
        return OfflineServiceInfo(
            id = id,
            insults = insults.map { InsultDto(it) }
        )
    }

}

open class OnlineInsultGeneratorService(
    override val id: String,
    private val baseUrl: String,
    private val languageParameterName: String?,
    private val extraQueryParams: Map<String, String>
) : InsultGeneratorService {

    private val client = HttpClient(getHttpClientEngineFactory())

    override suspend fun generate(language: Language): Insult {
        val response = client.get(baseUrl) {
            url {
                if (languageParameterName != null) {
                    parameters.append(languageParameterName, language.languageCode)
                }
                for ((name, value) in extraQueryParams) {
                    parameters.append(name, value)
                }
            }
        }
        return Json.decodeFromString(InsultSerializer, response.bodyAsText())
    }

    override fun getInfo(): ServiceInfo {
        return OnlineServiceInfo(
            id = id,
            baseUrl = baseUrl,
            languageParameterName = languageParameterName,
            extraQueryParams = extraQueryParams
        )
    }

}

class OfficialOnlineInsultGeneratorService(url: Url) : OnlineInsultGeneratorService(
    id = url.baseUrl,
    baseUrl = url.baseUrl,
    languageParameterName = "lang",
    extraQueryParams = mapOf("type" to "json")
) {

    enum class Url(internal val baseUrl: String) {
        MAIN(OFFICIAL_EIG_URL), BACKUP(OFFICIAL_EIG_BACKUP_URL)
    }
}

const val OFFICIAL_EIG_URL = "https://evilinsult.com/generate_insult.php"

const val OFFICIAL_EIG_BACKUP_URL = "https://slave.evilinsult.com/generate_insult.php"