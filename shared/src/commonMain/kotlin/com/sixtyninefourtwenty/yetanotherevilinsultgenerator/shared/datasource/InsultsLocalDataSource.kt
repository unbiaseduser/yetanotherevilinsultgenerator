package com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.datasource

import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.database.OfflineInsultsDatabase
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.dto.InsultDto
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.dto.InsultRatingDto
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

interface InsultsLocalDataSource {
    val allAvailableInsults: Flow<List<InsultDto>>
    suspend fun upsertInsult(insult: InsultDto)
    suspend fun deleteInsult(number: Int)
    suspend fun deleteInsult(insult: InsultDto) = deleteInsult(insult.number)
    suspend fun deleteInsults(numbers: IntArray)
    suspend fun deleteInsults(insults: Iterable<InsultDto>) {
        val iterator = insults.iterator()
        deleteInsults(IntArray(insults.count()) { iterator.next().number })
    }
    suspend fun updateFavoriteStatus(number: Int, isFavorite: Boolean)
    suspend fun updateFavoriteStatus(insult: InsultDto, isFavorite: Boolean) = updateFavoriteStatus(insult.number, isFavorite)
    fun getFavoriteStatus(number: Int): Flow<Boolean>
    fun getFavoriteStatus(insult: InsultDto): Flow<Boolean> = getFavoriteStatus(insult.number)
    fun getRating(number: Int): Flow<InsultRatingDto?>
    fun getRating(insult: InsultDto): Flow<InsultRatingDto?> = getRating(insult.number)
    suspend fun updateRating(number: Int, rating: InsultRatingDto?)
    suspend fun updateRating(insult: InsultDto, rating: InsultRatingDto?) = updateRating(insult.number, rating)
}

class InsultsLocalDataSourceImpl(private val db: OfflineInsultsDatabase) : InsultsLocalDataSource {
    override val allAvailableInsults = db.allAvailableInsults.map { insults -> insults.map { InsultDto(it) } }
    override suspend fun upsertInsult(insult: InsultDto) = db.upsertInsult(insult.toEntity())
    override suspend fun deleteInsult(number: Int) = db.deleteInsult(number)
    override suspend fun deleteInsults(numbers: IntArray) = db.deleteInsults(numbers)
    override suspend fun updateFavoriteStatus(number: Int, isFavorite: Boolean) = db.updateFavoriteStatus(number, isFavorite)
    override fun getFavoriteStatus(number: Int) = db.getFavoriteStatus(number)
    override fun getRating(number: Int) = db.getRating(number).map { it?.let { InsultRatingDto(it) } }
    override suspend fun updateRating(number: Int, rating: InsultRatingDto?) = db.updateRating(number, rating?.toEntity())
}