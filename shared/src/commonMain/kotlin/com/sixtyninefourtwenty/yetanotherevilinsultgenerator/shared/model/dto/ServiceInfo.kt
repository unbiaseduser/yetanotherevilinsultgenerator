package com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.dto

sealed interface ServiceInfo {
    val id: String
}