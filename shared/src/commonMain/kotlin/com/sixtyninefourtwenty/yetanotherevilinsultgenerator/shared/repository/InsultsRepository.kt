package com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.repository

import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.datasource.InsultsLocalDataSource
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.datasource.InsultsRemoteDataSource
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.Language
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.dto.InsultDto
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.dto.InsultRatingDto
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.withContext

interface InsultsRepository {
    val allAvailableLocalInsults: Flow<List<InsultDto>>
    val officialRemoteDataSources: List<InsultsRemoteDataSource>
    suspend fun deleteLocalInsult(number: Int)
    suspend fun deleteLocalInsult(insult: InsultDto) = deleteLocalInsult(insult.number)
    suspend fun deleteLocalInsults(numbers: IntArray)
    suspend fun deleteLocalInsults(insults: Iterable<InsultDto>) {
        val iterator = insults.iterator()
        deleteLocalInsults(IntArray(insults.count()) { iterator.next().number })
    }
    suspend fun upsertInsult(insult: InsultDto)
    suspend fun updateFavoriteStatus(number: Int, isFavorite: Boolean)
    suspend fun updateFavoriteStatus(insult: InsultDto, isFavorite: Boolean) = updateFavoriteStatus(insult.number, isFavorite)
    fun getFavoriteStatus(number: Int): Flow<Boolean>
    fun getFavoriteStatus(insult: InsultDto): Flow<Boolean> = getFavoriteStatus(insult.number)
    fun getRating(number: Int): Flow<InsultRatingDto?>
    fun getRating(insult: InsultDto): Flow<InsultRatingDto?> = getRating(insult.number)
    suspend fun updateRating(number: Int, rating: InsultRatingDto?)
    suspend fun updateRating(insult: InsultDto, rating: InsultRatingDto?) = updateRating(insult.number, rating)
    suspend fun generateRemoteInsult(id: String, language: Language): InsultDto
    val allCustomOnlineGenerators: Flow<List<InsultsRemoteDataSource>>
}

class InsultsRepositoryImpl(
    override val officialRemoteDataSources: List<InsultsRemoteDataSource>,
    private val customGeneratorInfoRepository: CustomGeneratorInfoRepository,
    private val localDataSource: InsultsLocalDataSource,
    private val scope: CoroutineScope
) : InsultsRepository {

    override val allAvailableLocalInsults = localDataSource.allAvailableInsults
    override suspend fun deleteLocalInsult(number: Int) = withContext(scope.coroutineContext) {
        localDataSource.deleteInsult(number)
    }
    override suspend fun deleteLocalInsults(numbers: IntArray) = withContext(scope.coroutineContext) {
        localDataSource.deleteInsults(numbers)
    }
    override suspend fun upsertInsult(insult: InsultDto) = withContext(scope.coroutineContext) {
        localDataSource.upsertInsult(insult)
    }
    override suspend fun updateFavoriteStatus(number: Int, isFavorite: Boolean) = withContext(scope.coroutineContext) {
        localDataSource.updateFavoriteStatus(number, isFavorite)
    }
    override fun getFavoriteStatus(number: Int) = localDataSource.getFavoriteStatus(number)
    override fun getRating(number: Int) = localDataSource.getRating(number)
    override suspend fun updateRating(number: Int, rating: InsultRatingDto?) = withContext(scope.coroutineContext) {
        localDataSource.updateRating(number, rating)
    }

    override suspend fun generateRemoteInsult(id: String, language: Language): InsultDto {
        val generator = officialRemoteDataSources.firstOrNull {
            id == it.id
        } ?: customGeneratorInfoRepository.getAllOnlineInfoSnapshot()
            .first { id == it.id }
            .toOnlineGenerator()
        return generator.generateRemoteInsult(language)
    }

    override val allCustomOnlineGenerators: Flow<List<InsultsRemoteDataSource>> = customGeneratorInfoRepository.allOnlineInfo.map { infos -> infos.map { it.toOnlineGenerator() } }

}