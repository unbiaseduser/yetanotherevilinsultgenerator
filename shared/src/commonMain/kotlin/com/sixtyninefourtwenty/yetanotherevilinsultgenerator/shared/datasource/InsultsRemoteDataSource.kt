package com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.datasource

import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.InsultGeneratorService
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.Language
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.dto.GeneratorInfo
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.dto.InsultDto
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.dto.OfflineGeneratorInfo
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.dto.OfflineServiceInfo
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.dto.OnlineGeneratorInfo
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.dto.OnlineServiceInfo

interface InsultsRemoteDataSource {
    val id: String
    val constantDisplayName: String?
    val dynamicDisplayName: (() -> String)?
    suspend fun generateRemoteInsult(language: Language): InsultDto
    fun getInfo(): GeneratorInfo

    fun getDisplayName() = constantDisplayName ?: dynamicDisplayName?.invoke() ?: error("Either constantDisplayName or dynamicDisplayName must be present")
}

class InsultsRemoteDataSourceImpl private constructor(
    override val constantDisplayName: String?,
    override val dynamicDisplayName: (() -> String)?,
    private val service: InsultGeneratorService
) : InsultsRemoteDataSource {

    constructor(
        constantDisplayName: String,
        service: InsultGeneratorService
    ) : this(
        constantDisplayName = constantDisplayName,
        dynamicDisplayName = null,
        service = service
    )

    constructor(
        dynamicDisplayName: () -> String,
        service: InsultGeneratorService
    ) : this(
        constantDisplayName = null,
        dynamicDisplayName = dynamicDisplayName,
        service = service
    )

    override val id: String
        get() = service.id

    override suspend fun generateRemoteInsult(language: Language) = InsultDto(service.generate(language))

    override fun getInfo(): GeneratorInfo {
        return when (val serviceInfo = service.getInfo()) {
            is OfflineServiceInfo -> OfflineGeneratorInfo(getDisplayName(), serviceInfo)
            is OnlineServiceInfo -> OnlineGeneratorInfo(getDisplayName(), serviceInfo)
            is OfflineGeneratorInfo -> serviceInfo
            is OnlineGeneratorInfo -> serviceInfo
        }
    }

}