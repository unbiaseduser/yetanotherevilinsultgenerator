package com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.dto

import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.InsultRating

data class InsultRatingDto(
    val rating: Float,
    val explanation: String?
) {

    internal constructor(insultRating: InsultRating) : this(
        rating = insultRating.rating,
        explanation = insultRating.explanation
    )

    internal fun toEntity() = InsultRating(
        rating = this.rating,
        explanation = this.explanation
    )

}
