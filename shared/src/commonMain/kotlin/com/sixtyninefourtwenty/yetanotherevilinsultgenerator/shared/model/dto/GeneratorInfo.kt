package com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.dto

sealed interface GeneratorInfo : ServiceInfo {
    val displayName: String
}