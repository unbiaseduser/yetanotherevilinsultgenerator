package com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model

import io.realm.kotlin.ext.realmDictionaryOf
import io.realm.kotlin.types.RealmDictionary
import io.realm.kotlin.types.RealmObject
import io.realm.kotlin.types.annotations.PrimaryKey

class OnlineGeneratorInfoEntity(
    @PrimaryKey
    var id: String,
    var baseUrl: String,
    var displayName: String,
    var languageParameterName: String?,
    var extraQueryParams: RealmDictionary<String>
) : RealmObject {

    @Suppress("unused")
    constructor() : this(
        id = "",
        baseUrl = "",
        displayName = "",
        languageParameterName = null,
        extraQueryParams = realmDictionaryOf()
    )

}