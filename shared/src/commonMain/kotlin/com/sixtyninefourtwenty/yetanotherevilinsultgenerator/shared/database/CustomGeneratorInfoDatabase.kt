package com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.database

import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.Insult
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.InsultRating
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.OfflineGeneratorInfoEntity
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.OnlineGeneratorInfoEntity
import io.realm.kotlin.RealmConfiguration
import io.realm.kotlin.UpdatePolicy
import io.realm.kotlin.ext.query
import kotlinx.coroutines.flow.map

class CustomGeneratorInfoDatabase private constructor(
    configuration: RealmConfiguration
) : RealmBackedDatabase(configuration) {

    companion object {
        private val schema = setOf(OnlineGeneratorInfoEntity::class, OfflineGeneratorInfoEntity::class, Insult::class, InsultRating::class)
        fun newInstance() = CustomGeneratorInfoDatabase(
            RealmConfiguration.Builder(schema)
                .name("custom_generator_info")
                .build()
        )
        @Suppress("unused")
        fun newInMemoryInstance() = CustomGeneratorInfoDatabase(
            RealmConfiguration.Builder(schema)
                .inMemory()
                .build()
        )
    }

    val allOnlineInfo = realm.query<OnlineGeneratorInfoEntity>().asFlow().map { it.list }

    fun getAllOnlineInfoSnapshot() = realm.query<OnlineGeneratorInfoEntity>().find()

    suspend fun upsertOnlineInfo(info: OnlineGeneratorInfoEntity) {
        realm.write {
            copyToRealm(info, UpdatePolicy.ALL)
        }
    }

    private fun queryOnlineInfoById(id: String) = realm.query<OnlineGeneratorInfoEntity>("id == $0", id).first()

    suspend fun deleteOnlineInfo(id: String) {
        val i = queryOnlineInfoById(id).find()
        if (i != null) {
            realm.write {
                findLatest(i)?.let { delete(it) }
            }
        }
    }

    suspend fun deleteOnlineInfo(info: OnlineGeneratorInfoEntity) {
        deleteOnlineInfo(info.id)
    }

}