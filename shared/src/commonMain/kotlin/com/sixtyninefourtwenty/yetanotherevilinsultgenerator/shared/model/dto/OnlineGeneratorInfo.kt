package com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.dto

import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.OnlineInsultGeneratorService
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.datasource.InsultsRemoteDataSource
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.datasource.InsultsRemoteDataSourceImpl
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.OnlineGeneratorInfoEntity
import io.realm.kotlin.ext.toRealmDictionary

data class OnlineGeneratorInfo(
    override val id: String,
    val baseUrl: String,
    override val displayName: String,
    val languageParameterName: String?,
    val extraQueryParams: Map<String, String>
) : GeneratorInfo {

    internal constructor(info: OnlineGeneratorInfoEntity) : this(
        id = info.id,
        baseUrl = info.baseUrl,
        displayName = info.displayName,
        languageParameterName = info.languageParameterName,
        extraQueryParams = info.extraQueryParams
    )

    internal constructor(
        displayName: String,
        serviceInfo: OnlineServiceInfo
    ) : this(
        id = serviceInfo.id,
        baseUrl = serviceInfo.baseUrl,
        displayName = displayName,
        languageParameterName = serviceInfo.languageParameterName,
        extraQueryParams = serviceInfo.extraQueryParams
    )

    internal fun toEntity() = OnlineGeneratorInfoEntity(
        id = this.id,
        baseUrl = this.baseUrl,
        displayName = this.displayName,
        languageParameterName = this.languageParameterName,
        extraQueryParams = this.extraQueryParams.toRealmDictionary()
    )

    fun toOnlineGenerator(): InsultsRemoteDataSource = InsultsRemoteDataSourceImpl(
        constantDisplayName = displayName,
        OnlineInsultGeneratorService(
            id, baseUrl, languageParameterName, extraQueryParams
        )
    )

}
