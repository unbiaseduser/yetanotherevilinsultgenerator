package com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.sortandfilter

import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.Language
import com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.dto.InsultDto

data class InsultFilterOptions(
    val language: Language?,
    val textFilter: TextFilter?
) {

    fun filterInsults(insults: List<InsultDto>): List<InsultDto> {
        var results = insults
        if (language != null) {
            results = results.filter { it.language == language }
        }
        if (textFilter != null) {
            results = results.filter { textFilter.testText(it.insult) }
        }
        return results
    }

}