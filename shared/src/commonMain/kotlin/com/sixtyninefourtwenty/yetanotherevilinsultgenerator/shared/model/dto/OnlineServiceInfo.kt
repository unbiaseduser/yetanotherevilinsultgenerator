package com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model.dto

data class OnlineServiceInfo(
    override val id: String,
    val baseUrl: String,
    val languageParameterName: String?,
    val extraQueryParams: Map<String, String>
) : ServiceInfo
