package com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared.model

import io.realm.kotlin.ext.realmListOf
import io.realm.kotlin.types.RealmList
import io.realm.kotlin.types.RealmObject
import io.realm.kotlin.types.annotations.PrimaryKey

class OfflineGeneratorInfoEntity(
    @PrimaryKey
    var id: String,
    var displayName: String,
    var insults: RealmList<Insult>
) : RealmObject {

    @Suppress("unused")
    constructor() : this(
        id = "",
        displayName = "",
        insults = realmListOf()
    )

}