package com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared

import kotlinx.datetime.LocalDateTime
import kotlinx.datetime.toJavaLocalDateTime
import kotlinx.datetime.toKotlinLocalDateTime
import java.time.format.DateTimeFormatter
import java.util.Locale
import java.time.LocalDateTime as JavaLocalDateTime

private const val INSULT_JSON_DATE_TIME_PATTERN = "uuuu-MM-dd HH:mm:ss"

private val INSULT_JSON_FIELD_DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern(INSULT_JSON_DATE_TIME_PATTERN, Locale.ENGLISH)

actual fun createLocalDateTimeFromInsultJsonField(str: String): LocalDateTime {
    return JavaLocalDateTime.parse(str, INSULT_JSON_FIELD_DATE_TIME_FORMATTER).toKotlinLocalDateTime()
}

actual fun createInsultJsonFieldFromLocalDateTime(dateTime: LocalDateTime): String {
    return INSULT_JSON_FIELD_DATE_TIME_FORMATTER.format(dateTime.toJavaLocalDateTime())
}
