package com.sixtyninefourtwenty.yetanotherevilinsultgenerator.shared

import android.annotation.SuppressLint
import android.os.Build
import io.ktor.client.engine.HttpClientEngine
import io.ktor.client.engine.HttpClientEngineFactory
import io.ktor.client.engine.okhttp.OkHttp
import io.ktor.client.engine.okhttp.OkHttpConfig
import okhttp3.CipherSuite
import okhttp3.ConnectionSpec
import okhttp3.TlsVersion
import java.security.cert.CertificateException
import java.security.cert.X509Certificate
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager

actual fun getHttpClientEngineFactory(): HttpClientEngineFactory<*> {
    return object : HttpClientEngineFactory<OkHttpConfig> {
        override fun create(block: OkHttpConfig.() -> Unit): HttpClientEngine {
            return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                // https://stackoverflow.com/a/50640113
                // FIXME Find out why this API 24 fix doesn't work. I hate Android fragmentation, seriously.
                if (Build.VERSION.SDK_INT == Build.VERSION_CODES.N) {
                    OkHttp.create {
                        block(this)
                        config {
                            connectionSpecs(listOf(ConnectionSpec.Builder(ConnectionSpec.COMPATIBLE_TLS)
                                .supportsTlsExtensions(true)
                                .tlsVersions(TlsVersion.TLS_1_2, TlsVersion.TLS_1_1, TlsVersion.TLS_1_0)
                                .cipherSuites(
                                    CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,
                                    CipherSuite.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
                                    CipherSuite.TLS_DHE_RSA_WITH_AES_128_GCM_SHA256,
                                    CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA,
                                    CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA,
                                    CipherSuite.TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA,
                                    CipherSuite.TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA,
                                    CipherSuite.TLS_ECDHE_ECDSA_WITH_RC4_128_SHA,
                                    CipherSuite.TLS_ECDHE_RSA_WITH_RC4_128_SHA,
                                    CipherSuite.TLS_DHE_RSA_WITH_AES_128_CBC_SHA,
                                    CipherSuite.TLS_DHE_DSS_WITH_AES_128_CBC_SHA,
                                    CipherSuite.TLS_DHE_RSA_WITH_AES_256_CBC_SHA)
                                .build()))
                        }
                    }
                } else {
                    OkHttp.create(block)
                }
            } else {
                // https://stackoverflow.com/a/60507560
                // Create a trust manager that does not validate certificate chains
                val trustAllCerts: Array<TrustManager> = arrayOf(
                    @SuppressLint("CustomX509TrustManager")
                    object : X509TrustManager {
                        @Throws(CertificateException::class)
                        override fun checkClientTrusted(chain: Array<X509Certificate?>?,
                                                        authType: String?) = Unit

                        @Throws(CertificateException::class)
                        override fun checkServerTrusted(chain: Array<X509Certificate?>?,
                                                        authType: String?) = Unit

                        override fun getAcceptedIssuers(): Array<X509Certificate> = arrayOf()
                    }
                )
                // Install the all-trusting trust manager
                val sslContext: SSLContext = SSLContext.getInstance("SSL")
                sslContext.init(null, trustAllCerts, null)
                // Create an ssl socket factory with our all-trusting manager
                val sslSocketFactory = sslContext.socketFactory
                OkHttp.create {
                    block(this)
                    config {
                        sslSocketFactory(sslSocketFactory, trustAllCerts[0] as X509TrustManager)
                    }
                }
            }
        }
    }
}
